/*
 Navicat Premium Data Transfer

 Source Server         : LearnBases
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : wx_house_production

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/05/2022 15:39:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `houseId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1210000004 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES (1210000001, 'UID1651743691600', 'HD165178800001', '0');
INSERT INTO `collect` VALUES (1210000002, 'UID1651743691600', 'HD165178800002', '0');
INSERT INTO `collect` VALUES (1210000003, 'UID1651743691600', 'HD165178800003', '0');

-- ----------------------------
-- Table structure for facilitylist
-- ----------------------------
DROP TABLE IF EXISTS `facilitylist`;
CREATE TABLE `facilitylist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `facilityId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设施编码',
  `facilityName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设施名称',
  `facilityType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设施类型 包含：基础设施 basic、洗浴设施 bath、厨房设施 kitchen、室外设施 outdoor、安全设施 safe、其他服务 other',
  `facilityUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设施图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30000024 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of facilitylist
-- ----------------------------
INSERT INTO `facilitylist` VALUES (30000001, 'FD16830000001', '大床', 'basic', '图片上传后勾选补充，或者直接保存再项目中，后台不做管理');
INSERT INTO `facilitylist` VALUES (30000002, 'FD16830000002', '空调', 'basic', NULL);
INSERT INTO `facilitylist` VALUES (30000003, 'FD16830000003', 'WIFI', 'basic', NULL);
INSERT INTO `facilitylist` VALUES (30000004, 'FD16830000004', '暖气', 'basic', NULL);
INSERT INTO `facilitylist` VALUES (30000005, 'FD16830000005', '沙发', 'basic', NULL);
INSERT INTO `facilitylist` VALUES (30000006, 'FD16830000006', '餐桌', 'basic', NULL);
INSERT INTO `facilitylist` VALUES (30000007, 'FD16830000007', '电梯', 'basic', NULL);
INSERT INTO `facilitylist` VALUES (30000008, 'FD16830000008', '电视', 'basic', NULL);
INSERT INTO `facilitylist` VALUES (30000009, 'FD16830000009', '热水器', 'bath', NULL);
INSERT INTO `facilitylist` VALUES (30000010, 'FD16830000010', '洗衣机', 'bath', NULL);
INSERT INTO `facilitylist` VALUES (30000011, 'FD16830000011', '卫生间', 'bath', NULL);
INSERT INTO `facilitylist` VALUES (30000012, 'FD16830000012', '冰箱', 'kitchen', NULL);
INSERT INTO `facilitylist` VALUES (30000013, 'FD16830000013', '油烟机', 'kitchen', NULL);
INSERT INTO `facilitylist` VALUES (30000014, 'FD16830000014', '微波炉', 'kitchen', NULL);
INSERT INTO `facilitylist` VALUES (30000015, 'FD16830000015', '健身房', 'outdoor', NULL);
INSERT INTO `facilitylist` VALUES (30000016, 'FD16830000016', '公交车', 'outdoor', NULL);
INSERT INTO `facilitylist` VALUES (30000017, 'FD16830000017', '地铁', 'outdoor', NULL);
INSERT INTO `facilitylist` VALUES (30000018, 'FD16830000018', '快递驿站', 'outdoor', NULL);
INSERT INTO `facilitylist` VALUES (30000019, 'FD16830000019', '超市', 'outdoor', NULL);
INSERT INTO `facilitylist` VALUES (30000020, 'FD16830000020', '充电桩', 'outdoor', NULL);
INSERT INTO `facilitylist` VALUES (30000021, 'FD16830000021', '烟雾报警', 'safe', NULL);
INSERT INTO `facilitylist` VALUES (30000022, 'FD16830000022', '消防栓', 'safe', NULL);
INSERT INTO `facilitylist` VALUES (30000023, 'FD16830000023', '可开发票', 'other', NULL);

-- ----------------------------
-- Table structure for houselist
-- ----------------------------
DROP TABLE IF EXISTS `houselist`;
CREATE TABLE `houselist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `houseId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓编码',
  `houseName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓名称',
  `houseLongName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓长名称',
  `houseLongitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓经度',
  `houseLatitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓纬度',
  `housePlace` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓地址',
  `landlordId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房东编码 用房东表选择',
  `houseCity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓省市（暂时没用到）后续推广后增加根据区域推荐优秀房源',
  `advantageList` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '优点列表',
  `houseTypeListId` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '拥有房型',
  `lessPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '最低价格',
  `houseCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓房码',
  `lessRentType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最低押金',
  `houseMainUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间主图片，用来热门推荐和收藏时显示',
  `isHot` tinyint(1) NULL DEFAULT 0 COMMENT '布尔类型 是否热门',
  `updateTime` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `createTime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8800005 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of houselist
-- ----------------------------
INSERT INTO `houselist` VALUES (8800001, 'HD165178800001', '舒鑫佳苑', '《 舒鑫佳苑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.13805', '24.523646', '福建省厦门市湖里区林后社555号林后小区东门', 'LD1651743698686', '福建省-厦门市-湖里区', '[\'电子门锁\',\'隔音好\',\'领包入住\',\'免物业\',\'靠近地铁\',\'相邻超市\',\'可做饭\']', NULL, 1450.00, '8001', '押一付一', NULL, 0, NULL, '2022-05-30 11:16:53');
INSERT INTO `houselist` VALUES (8800002, 'HD165178800002', '百顺公寓', '《 百顺公寓 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.144585', '24.529944', '福建省厦门市湖里区围里社1099号楼', 'LD1651743698687', '福建省-厦门市-湖里区', '[\'电子门锁\',\'隔音好\',\'领包入住\',\'免物业\',\'靠近地铁\',\'相邻超市\',\'可做饭\']', NULL, 1350.00, '8002', '押一付一', NULL, 0, NULL, '2022-05-30 11:16:53');
INSERT INTO `houselist` VALUES (8800003, 'HD165178800003', '永建顶尚', '《 永建顶尚 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.150131', '24.51308', '福建省厦门市湖里区枋湖南路185号', 'LD1651743698688', '福建省-厦门市-湖里区', '[\'电子门锁\',\'隔音好\',\'领包入住\',\'免物业\',\'靠近地铁\',\'相邻超市\',\'可做饭\']', NULL, 1650.00, '8003', '押一付三', NULL, 1, NULL, '2022-05-30 11:16:53');
INSERT INTO `houselist` VALUES (8800004, 'HD165178800004', '闲寓·筱筑', '《 闲寓·筱筑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.150612', '24.524498', '福建省厦门市湖里区县后社391号', 'LD1651743698689', '福建省-厦门市-湖里区', '[\'电子门锁\',\'隔音好\',\'领包入住\',\'免物业\',\'靠近地铁\',\'相邻超市\',\'可做饭\']', NULL, 1800.00, '8004', '押二付一', NULL, 0, NULL, '2022-05-30 11:16:53');

-- ----------------------------
-- Table structure for imglist
-- ----------------------------
DROP TABLE IF EXISTS `imglist`;
CREATE TABLE `imglist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `imgId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间图片编码',
  `imgUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片地址',
  `imgName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片名称',
  `imgRoomName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片房间名称',
  `imgType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片类型 包含1：3D图、2：主图片、3：图标图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 600005 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of imglist
-- ----------------------------
INSERT INTO `imglist` VALUES (600001, 'ID16496657006', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', 'XX公寓一室一厅客厅图片', '客厅', '1');
INSERT INTO `imglist` VALUES (600002, 'ID16496657007', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', 'XX公寓一室一厅厨房图片', '厨房', '1');
INSERT INTO `imglist` VALUES (600003, 'ID16496657008', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', '永建顶尚一室一厅主图片', '', '2');
INSERT INTO `imglist` VALUES (600004, 'ID16496657009', 'https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large', '永建顶尚单间主图片', NULL, '2');

-- ----------------------------
-- Table structure for landlorduser
-- ----------------------------
DROP TABLE IF EXISTS `landlorduser`;
CREATE TABLE `landlorduser`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `landlordId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房东编码',
  `landlordName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房东名称',
  `landlordPhone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房东电话',
  `landlordDetail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房东描述',
  `landlordPassword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '222666' COMMENT '房东密码 默认222666',
  `landlordType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '0新房东，1 超赞房东，2 实惠房东',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1004 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of landlorduser
-- ----------------------------
INSERT INTO `landlorduser` VALUES (1000, 'LD1651743698686', '廖房东', '15259788888', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');
INSERT INTO `landlorduser` VALUES (1001, 'LD1651743698687', '陈房东', '15259888886', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');
INSERT INTO `landlorduser` VALUES (1002, 'LD1651743698688', '李华容', '15486798686', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');
INSERT INTO `landlorduser` VALUES (1003, 'LD1651743698689', '龙化叶', '15486896622', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');

-- ----------------------------
-- Table structure for manageruser
-- ----------------------------
DROP TABLE IF EXISTS `manageruser`;
CREATE TABLE `manageruser`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '用户账号状态 0正常 1禁用',
  `identity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '用户身份 0管理员 1用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manageruser
-- ----------------------------
INSERT INTO `manageruser` VALUES (1, 'admin', 'admin@2022.jyf', '89b90b801816e09cc7ab68820d54fb28', '0', '0');
INSERT INTO `manageruser` VALUES (6, 'UID1651218095985', 'test26', 'd30ceacf031d9d9d2764d726d8ad1652', '0', '1');

-- ----------------------------
-- Table structure for roomlist
-- ----------------------------
DROP TABLE IF EXISTS `roomlist`;
CREATE TABLE `roomlist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `roomId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间编码',
  `houseId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓编码',
  `facilityList` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '设施列表',
  `roomRent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公寓租金',
  `roomRentType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间押金',
  `roomList` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间空间图片 暂时不用',
  `roomTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间标题',
  `roomType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间类型',
  `roomMainUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间主图片',
  `mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 800000003 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomlist
-- ----------------------------
INSERT INTO `roomlist` VALUES (680000001, 'RD16868000001', 'HD165178800003', '[\'大床\',\'空调\']', '1650', '押一付一', '暂不填', '永建顶尚 一室一厅 （位于2、3和5层）', '一室一厅', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', NULL);
INSERT INTO `roomlist` VALUES (800000002, 'RD16868000002', 'HD165178800003', '[\'大床\',\'空调\']', '1300', '押一付一', NULL, '永建顶尚 大单间 （位于1、4和6层）', '大单间', 'https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large', NULL);

-- ----------------------------
-- Table structure for roomplacelist
-- ----------------------------
DROP TABLE IF EXISTS `roomplacelist`;
CREATE TABLE `roomplacelist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `roomPlaceId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间具体地方编码',
  `roomId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间编码',
  `roomPlaceName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间具体地方名称 例如客厅 厨房等',
  `roomPlaceImg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房间具体地方图片',
  `mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 660000003 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomplacelist
-- ----------------------------
INSERT INTO `roomplacelist` VALUES (660000001, 'RPD16866000001', 'RD16868000001', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '永建顶尚 一室一厅 客厅展示');
INSERT INTO `roomplacelist` VALUES (660000002, 'RPD16866000002', 'RD16868000001', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '永建顶尚 一室一厅 厨房展示');

-- ----------------------------
-- Table structure for wxuser
-- ----------------------------
DROP TABLE IF EXISTS `wxuser`;
CREATE TABLE `wxuser`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatarUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nickName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '默认0',
  `createTime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wxuser
-- ----------------------------
INSERT INTO `wxuser` VALUES (1, 'UID1651743691600', 'onYN65JRB9vNp101pMoIG8GCG6Vs', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIamF3jz3e9htITltxBPiau218zSTgCjXQWQEYZY70xW0WYDX1GXQxN5QAsynyHomQ2qEh6VZTmlOg/132', '_WaCz', 'nullGender', '0', '2022-05-30 11:15:40', NULL);

SET FOREIGN_KEY_CHECKS = 1;
