-- MySQL dump 10.13  Distrib 5.5.62, for Linux (x86_64)
--
-- Host: localhost    Database: wx_house
-- ------------------------------------------------------
-- Server version	5.5.62-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `imglist`
--

DROP TABLE IF EXISTS `imglist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imglist` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '该表用在图片可以复用时，关联表',
  `img_Id` varchar(255) DEFAULT NULL,
  `imgUrl` varchar(255) DEFAULT NULL,
  `tips_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imglist`
--

LOCK TABLES `imglist` WRITE;
/*!40000 ALTER TABLE `imglist` DISABLE KEYS */;
INSERT INTO `imglist` VALUES (19,'img_1649988248096','http://110.42.186.39:3030/public/upload/images/rn8J7pAhMyEpG5FImlA_xNcp.png',NULL),(20,'img_1649988349224','http://110.42.186.39:3030/public/upload/images/Acqwz__3a2221J87LWFMCgjv.jpg',NULL),(21,'img_1649988976780','http://110.42.186.39:3030/public/upload/images/-KljkxZBD6UmuRnha2sbcZlj.jpg',NULL),(22,'img_1649993331703','http://110.42.186.39:3030/public/upload/images/X57rPSo5hqqUFh-8hW6S42L9.png',NULL),(23,'img_1650010543950','http://110.42.186.39:3030/public/upload/images/Tpb2CX7yHEm0uZdiIsK5U5mO.jpg',NULL),(24,'img_1650010981614','http://110.42.186.39:3030/public/upload/images/0yINJL-TzpEg9sGoKP3wWiS3.jpg',NULL),(25,'img_1650011457305','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg','tips1649989032987'),(26,'img_1651127814213','http://110.42.186.39:3030/public/upload/images/DRYoetN7CdqqRhkFdLegqJGN.jpg',NULL),(27,'img_1651128301978','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg','tips1649988594739');
/*!40000 ALTER TABLE `imglist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pclist`
--

DROP TABLE IF EXISTS `pclist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pclist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pc_id` varchar(255) DEFAULT NULL COMMENT 'position和content的集合',
  `x` varchar(255) DEFAULT NULL,
  `y` varchar(255) DEFAULT NULL,
  `z` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `showTip` tinyint(1) DEFAULT NULL COMMENT '布尔类型',
  `showTitle` tinyint(1) DEFAULT NULL COMMENT '布尔类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pclist`
--

LOCK TABLES `pclist` WRITE;
/*!40000 ALTER TABLE `pclist` DISABLE KEYS */;
INSERT INTO `pclist` VALUES (1,'pc_16496657001','-200','-4','-147','进入厨房',NULL,'1',1,1),(2,'pc_16496657002','-100','0','-231','信息点2','77989',NULL,1,1),(3,'pc_16496657003','150','-50','-198','信息点3','qwdcz',NULL,1,1),(4,'pc_16496657004','210','11','-140','信息点4','大豆食心虫侦察十大大苏打大大大大大大大',NULL,1,1),(5,'pc_16496657005','208','-12','140','信息点5','eq',NULL,1,1),(6,'pc_16496657006','86','-9','236','进入房间',NULL,NULL,1,1),(16,'pc_1649840009869','-199','-24','145','进入大厅','无无无','0',0,1);
/*!40000 ALTER TABLE `pclist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tips_img_relation`
--

DROP TABLE IF EXISTS `tips_img_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tips_img_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tips_id` varchar(255) DEFAULT NULL,
  `img_Id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tips_img_relation`
--

LOCK TABLES `tips_img_relation` WRITE;
/*!40000 ALTER TABLE `tips_img_relation` DISABLE KEYS */;
INSERT INTO `tips_img_relation` VALUES (1,'该表用在图片可以复用时，关联表','现在设计暂时图片和房间1对1 无需关联');
/*!40000 ALTER TABLE `tips_img_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tips_pc_relation`
--

DROP TABLE IF EXISTS `tips_pc_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tips_pc_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tips_id` varchar(255) DEFAULT NULL,
  `pc_id` varchar(255) DEFAULT NULL,
  `img_id` varchar(255) DEFAULT NULL COMMENT '该字段作废',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tips_pc_relation`
--

LOCK TABLES `tips_pc_relation` WRITE;
/*!40000 ALTER TABLE `tips_pc_relation` DISABLE KEYS */;
INSERT INTO `tips_pc_relation` VALUES (18,'tips1649988594739','pc_16496657001',NULL),(19,'tips1649988594739','pc_16496657002',NULL),(20,'tips1649988594739','pc_16496657003',NULL),(21,'tips1649988594739','pc_16496657004',NULL),(23,'tips1649988594739','pc_16496657005',NULL),(24,'tips1649988594739','pc_16496657006',NULL),(25,'tips1649989032987','pc_1649840009869',NULL);
/*!40000 ALTER TABLE `tips_pc_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipslist`
--

DROP TABLE IF EXISTS `tipslist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tips_id` varchar(255) DEFAULT NULL,
  `room_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipslist`
--

LOCK TABLES `tipslist` WRITE;
/*!40000 ALTER TABLE `tipslist` DISABLE KEYS */;
INSERT INTO `tipslist` VALUES (14,'tips1649988594739','房间'),(15,'tips1649989032987','厨房');
/*!40000 ALTER TABLE `tipslist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0' COMMENT '用户账号状态 0正常 1禁用',
  `identity` varchar(255) NOT NULL DEFAULT '1' COMMENT '用户身份 0管理员 1用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin@2022.jyf','89b90b801816e09cc7ab68820d54fb28','0','0'),(6,'UID1651218095985','test26','d30ceacf031d9d9d2764d726d8ad1652','0','1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'wx_house'
--

--
-- Dumping routines for database 'wx_house'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-23 16:57:07
