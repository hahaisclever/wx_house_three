-- MySQL dump 10.13  Distrib 5.5.62, for Linux (x86_64)
--
-- Host: localhost    Database: wx_house_pro
-- ------------------------------------------------------
-- Server version	5.5.62-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `collect`
--

DROP TABLE IF EXISTS `collect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `houseId` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1210000004 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collect`
--

LOCK TABLES `collect` WRITE;
/*!40000 ALTER TABLE `collect` DISABLE KEYS */;
INSERT INTO `collect` VALUES (1210000001,'UID1651743691600','HD165178800001','0'),(1210000002,'UID1651743691600','HD165178800002','0'),(1210000003,'UID1651743691600','HD165178800003','0');
/*!40000 ALTER TABLE `collect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facilitylist`
--

DROP TABLE IF EXISTS `facilitylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facilitylist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facilityId` varchar(255) DEFAULT NULL COMMENT '设施编码',
  `facilityName` varchar(255) DEFAULT NULL COMMENT '设施名称',
  `facilityType` varchar(255) DEFAULT NULL COMMENT '设施类型 包含：基础设施 basic、洗浴设施 bath、厨房设施 kitchen、室外设施 outdoor、安全设施 safe、其他服务 other',
  `facilityUrl` varchar(255) DEFAULT NULL COMMENT '设施图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30000024 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facilitylist`
--

LOCK TABLES `facilitylist` WRITE;
/*!40000 ALTER TABLE `facilitylist` DISABLE KEYS */;
INSERT INTO `facilitylist` VALUES (30000001,'FD16830000001','大床','basic','图片上传后勾选补充，或者直接保存再项目中，后台不做管理'),(30000002,'FD16830000002','空调','basic',NULL),(30000003,'FD16830000003','WIFI','basic',NULL),(30000004,'FD16830000004','暖气','basic',NULL),(30000005,'FD16830000005','沙发','basic',NULL),(30000006,'FD16830000006','餐桌','basic',NULL),(30000007,'FD16830000007','电梯','basic',NULL),(30000008,'FD16830000008','电视','basic',NULL),(30000009,'FD16830000009','热水器','bath',NULL),(30000010,'FD16830000010','洗衣机','bath',NULL),(30000011,'FD16830000011','卫生间','bath',NULL),(30000012,'FD16830000012','冰箱','kitchen',NULL),(30000013,'FD16830000013','油烟机','kitchen',NULL),(30000014,'FD16830000014','微波炉','kitchen',NULL),(30000015,'FD16830000015','健身房','outdoor',NULL),(30000016,'FD16830000016','公交车','outdoor',NULL),(30000017,'FD16830000017','地铁','outdoor',NULL),(30000018,'FD16830000018','快递驿站','outdoor',NULL),(30000019,'FD16830000019','超市','outdoor',NULL),(30000020,'FD16830000020','充电桩','outdoor',NULL),(30000021,'FD16830000021','烟雾报警','safe',NULL),(30000022,'FD16830000022','消防栓','safe',NULL),(30000023,'FD16830000023','可开发票','other',NULL);
/*!40000 ALTER TABLE `facilitylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `houselist`
--

DROP TABLE IF EXISTS `houselist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `houselist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `houseId` varchar(255) DEFAULT NULL COMMENT '公寓编码',
  `houseName` varchar(255) DEFAULT NULL COMMENT '公寓名称',
  `houseLongName` varchar(255) DEFAULT NULL COMMENT '公寓长名称',
  `houseLongitude` varchar(255) DEFAULT NULL COMMENT '公寓经度',
  `houseLatitude` varchar(255) DEFAULT NULL COMMENT '公寓纬度',
  `housePlace` varchar(255) DEFAULT NULL COMMENT '公寓地址',
  `landlordId` varchar(255) DEFAULT NULL COMMENT '房东编码 用房东表选择',
  `houseCity` varchar(255) DEFAULT NULL COMMENT '公寓省市（暂时没用到）后续推广后增加根据区域推荐优秀房源',
  `advantageList` longtext COMMENT '优点列表',
  `houseTypeListId` longtext COMMENT '拥有房型',
  `lessPrice` decimal(10,2) DEFAULT NULL COMMENT '最低价格',
  `houseCode` varchar(255) DEFAULT NULL COMMENT '公寓房码',
  `lessRentType` varchar(255) DEFAULT NULL COMMENT '最低押金',
  `houseMainUrl` varchar(255) DEFAULT NULL COMMENT '房间主图片，用来热门推荐和收藏时显示',
  `isHot` tinyint(1) DEFAULT '0' COMMENT '布尔类型 是否热门',
  `houseOwnType` varchar(255) DEFAULT NULL COMMENT '2022-06-01新增属性：公寓拥有类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8800007 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `houselist`
--

LOCK TABLES `houselist` WRITE;
/*!40000 ALTER TABLE `houselist` DISABLE KEYS */;
INSERT INTO `houselist` VALUES (8800001,'HD165178800001','舒鑫佳苑','《 舒鑫佳苑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.13805','24.523646','福建省厦门市湖里区林后社555号林后小区东门','LD1651743698686','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,1650.00,'8001','押一付一','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',0,'LOFT,两室一厅'),(8800002,'HD165178800002','百顺公寓','《 百顺公寓 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.144585','24.529944','福建省厦门市湖里区围里社1099号楼','LD1651743698687','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,1350.00,'8002','押一付一','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',0,'LOFT,一室一厅'),(8800003,'HD165178800003','永建顶尚','《 永建顶尚 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.150131','24.51308','福建省厦门市湖里区枋湖南路185号','LD1651743698688','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,1300.00,'8003','押一付三','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',1,'大单间,loft'),(8800004,'HD165178800004','闲寓·筱筑','《 闲寓·筱筑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.150612','24.524498','福建省厦门市湖里区县后社391号','LD1651743698689','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,2100.00,'8004','押二付一','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',1,'两室一厅,三室一厅');
/*!40000 ALTER TABLE `houselist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `houselist_backups`
--

DROP TABLE IF EXISTS `houselist_backups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `houselist_backups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `houseId` varchar(255) DEFAULT NULL COMMENT '公寓编码',
  `houseName` varchar(255) DEFAULT NULL COMMENT '公寓名称',
  `houseLongName` varchar(255) DEFAULT NULL COMMENT '公寓长名称',
  `houseLongitude` varchar(255) DEFAULT NULL COMMENT '公寓经度',
  `houseLatitude` varchar(255) DEFAULT NULL COMMENT '公寓纬度',
  `housePlace` varchar(255) DEFAULT NULL COMMENT '公寓地址',
  `landlordId` varchar(255) DEFAULT NULL COMMENT '房东编码 用房东表选择',
  `houseCity` varchar(255) DEFAULT NULL COMMENT '公寓省市（暂时没用到）后续推广后增加根据区域推荐优秀房源',
  `advantageList` longtext COMMENT '优点列表',
  `houseTypeListId` longtext COMMENT '拥有房型',
  `lessPrice` decimal(10,2) DEFAULT NULL COMMENT '最低价格',
  `houseCode` varchar(255) DEFAULT NULL COMMENT '公寓房码',
  `lessRentType` varchar(255) DEFAULT NULL COMMENT '最低押金',
  `houseMainUrl` varchar(255) DEFAULT NULL COMMENT '房间主图片，用来热门推荐和收藏时显示',
  `isHot` tinyint(1) DEFAULT '0' COMMENT '布尔类型 是否热门',
  `houseOwnType` varchar(255) DEFAULT NULL COMMENT '2022-06-01新增属性：公寓拥有类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8800007 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `houselist_backups`
--

LOCK TABLES `houselist_backups` WRITE;
/*!40000 ALTER TABLE `houselist_backups` DISABLE KEYS */;
INSERT INTO `houselist_backups` VALUES (8800001,'HD165178800001','舒鑫佳苑','《 舒鑫佳苑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.13805','24.523646','福建省厦门市湖里区林后社555号林后小区东门','LD1651743698686','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,1650.00,'8001','押一付一','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',0,'LOFT,两室一厅'),(8800002,'HD165178800002','百顺公寓','《 百顺公寓 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.144585','24.529944','福建省厦门市湖里区围里社1099号楼','LD1651743698687','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,1350.00,'8002','押一付一','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',0,'LOFT,一室一厅'),(8800003,'HD165178800003','永建顶尚','《 永建顶尚 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.150131','24.51308','福建省厦门市湖里区枋湖南路185号','LD1651743698688','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,1300.00,'8003','押一付三','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',1,'大单间,loft'),(8800004,'HD165178800004','闲寓·筱筑','《 闲寓·筱筑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.150612','24.524498','福建省厦门市湖里区县后社391号','LD1651743698689','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,2100.00,'8004','押二付一','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',1,'两室一厅,三室一厅'),(8800005,'HD1654155649604','闲寓·筱筑15','《 闲寓·筱筑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站','118.150612','24.524498','福建省厦门市湖里区县后社391号','LD1651743698689','福建省-厦门市-湖里区','电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭',NULL,NULL,'8005','押二付一','https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large',0,NULL),(8800006,'HD1654929764978','测试新增公寓','测试新增公寓长标题','118.13805','24.523646','福建福州','LD1651743698686','福建省-厦门市-湖里区','[\'XXXX\',\'XXXX\']',NULL,1350.00,'8006','押一付一','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg',0,'LOFT,LOFT');
/*!40000 ALTER TABLE `houselist_backups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imglist`
--

DROP TABLE IF EXISTS `imglist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imglist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgId` varchar(255) DEFAULT NULL COMMENT '房间图片编码',
  `imgUrl` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `imgName` varchar(255) DEFAULT NULL COMMENT '图片名称',
  `imgRoomName` varchar(255) DEFAULT NULL COMMENT '图片房间名称',
  `imgType` varchar(255) DEFAULT NULL COMMENT '图片类型 包含1：3D图、2：主图片、3：图标图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=600005 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imglist`
--

LOCK TABLES `imglist` WRITE;
/*!40000 ALTER TABLE `imglist` DISABLE KEYS */;
INSERT INTO `imglist` VALUES (600001,'ID16496657006','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg','XX公寓一室一厅客厅图片','客厅','1'),(600002,'ID16496657007','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg','XX公寓一室一厅厨房图片','厨房','1'),(600003,'ID16496657008','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large','永建顶尚一室一厅主图片','','2'),(600004,'ID16496657009','https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large','永建顶尚单间主图片',NULL,'2');
/*!40000 ALTER TABLE `imglist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landlorduser`
--

DROP TABLE IF EXISTS `landlorduser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `landlorduser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `landlordId` varchar(255) DEFAULT NULL COMMENT '房东编码',
  `landlordName` varchar(255) DEFAULT NULL COMMENT '房东名称',
  `landlordPhone` varchar(255) DEFAULT NULL COMMENT '房东电话',
  `landlordDetail` varchar(255) DEFAULT NULL COMMENT '房东描述',
  `landlordPassword` varchar(255) DEFAULT '222666' COMMENT '房东密码 默认222666',
  `landlordType` varchar(255) DEFAULT '0' COMMENT '0新房东，1 超赞房东，2 实惠房东',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1004 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landlorduser`
--

LOCK TABLES `landlorduser` WRITE;
/*!40000 ALTER TABLE `landlorduser` DISABLE KEYS */;
INSERT INTO `landlorduser` VALUES (1000,'LD1651743698686','廖房东','15259788888','超赞房东经验丰富，致力于为房客提供优质的住宿体验。','222666','0'),(1001,'LD1651743698687','陈房东','15259888886','超赞房东经验丰富，致力于为房客提供优质的住宿体验。','222666','0'),(1002,'LD1651743698688','李华容','15486798686','超赞房东经验丰富，致力于为房客提供优质的住宿体验。','222666','0'),(1003,'LD1651743698689','龙化叶','15486896622','超赞房东经验丰富，致力于为房客提供优质的住宿体验。','222666','0');
/*!40000 ALTER TABLE `landlorduser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomlist`
--

DROP TABLE IF EXISTS `roomlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roomlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomId` varchar(255) DEFAULT NULL COMMENT '房间编码',
  `houseId` varchar(255) DEFAULT NULL COMMENT '公寓编码',
  `facilityList` longtext COMMENT '设施列表',
  `roomRent` varchar(255) DEFAULT NULL COMMENT '公寓租金',
  `roomRentType` varchar(255) DEFAULT NULL COMMENT '房间押金',
  `roomList` varchar(255) DEFAULT NULL COMMENT '房间空间图片 暂时不用',
  `roomTitle` varchar(255) DEFAULT NULL COMMENT '房间标题',
  `roomType` varchar(255) DEFAULT NULL COMMENT '房间类型',
  `roomMainUrl` varchar(255) DEFAULT NULL COMMENT '房间主图片',
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=800000010 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomlist`
--

LOCK TABLES `roomlist` WRITE;
/*!40000 ALTER TABLE `roomlist` DISABLE KEYS */;
INSERT INTO `roomlist` VALUES (680000001,'RD16868000001','HD165178800001','[\"FD16830000001\",\"FD16830000002\",\"FD16830000003\",\"FD16830000004\",\"FD16830000005\",\"FD16830000006\"]','1650','押一付一','暂不填','测试舒鑫佳苑LOFT','LOFT','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',NULL),(800000002,'RD16868000002','HD165178800003','[\"FD16830000001\",\"FD16830000002\"]','1300','押一付一',NULL,'永建顶尚 大单间 （位于1、4和6层）','大单间','https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large',NULL),(800000003,'RD1654152157975','HD165178800003','[\"FD16830000001\",\"FD16830000002\",\"FD16830000003\",\"FD16830000004\",\"FD16830000005\"]','1450','押一付一',NULL,'永建顶尚 loft （位于8和12层）','loft','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',NULL),(800000005,'RD1654936992988','HD165178800004','[\"FD16830000001\",\"FD16830000002\",\"FD16830000004\"]','2100','押一付一',NULL,'测试闲寓两室一厅','两室一厅','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',NULL),(800000006,'RD1655802627140','HD165178800001','[\"FD16830000001\",\"FD16830000004\",\"FD16830000002\",\"FD16830000003\",\"FD16830000005\",\"FD16830000006\"]','1850','押一付一',NULL,'测试舒鑫佳苑两室一厅','两室一厅','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',NULL),(800000007,'RD1655802724886','HD165178800002','[\"FD16830000001\",\"FD16830000002\",\"FD16830000003\",\"FD16830000004\",\"FD16830000005\",\"FD16830000006\"]','1350','押一付一',NULL,'测试百顺公寓LOFT','LOFT','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',NULL),(800000008,'RD1655802749958','HD165178800002','[\"FD16830000011\",\"FD16830000013\",\"FD16830000002\",\"FD16830000001\",\"FD16830000005\"]','1450','押一付一',NULL,'测试百顺公寓一室一厅','一室一厅','https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large',NULL),(800000009,'RD1655802837265','HD165178800004','[\"FD16830000002\",\"FD16830000007\",\"FD16830000004\",\"FD16830000003\",\"FD16830000001\",\"FD16830000005\",\"FD16830000006\",\"FD16830000008\"]','3000','押一付一',NULL,'测试闲寓三室一厅','三室一厅','https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large',NULL);
/*!40000 ALTER TABLE `roomlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomplacelist`
--

DROP TABLE IF EXISTS `roomplacelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roomplacelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomPlaceId` varchar(255) DEFAULT NULL COMMENT '房间具体地方编码',
  `roomId` varchar(255) DEFAULT NULL COMMENT '房间编码',
  `roomPlaceName` varchar(255) DEFAULT NULL COMMENT '房间具体地方名称 例如客厅 厨房等',
  `roomPlaceImg` varchar(255) DEFAULT NULL COMMENT '房间具体地方图片',
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=660000018 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomplacelist`
--

LOCK TABLES `roomplacelist` WRITE;
/*!40000 ALTER TABLE `roomplacelist` DISABLE KEYS */;
INSERT INTO `roomplacelist` VALUES (660000001,'RPD16866000001','RD16868000001','客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg','永建顶尚 一室一厅 客厅展示'),(660000002,'RPD16866000002','RD16868000001','厨房','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg','永建顶尚 一室一厅 厨房展示'),(660000003,'RPD1654940017483','RD1654936992988','客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg','测试客厅'),(660000005,'RPD1655733567310','RD16868000002','房间客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg','房间客厅'),(660000006,'RPD1655733576091','RD16868000002','房间厨房','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg','房间厨房'),(660000007,'RPD1655802406583','RD1654152157975','客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg',''),(660000008,'RPD1655802418313','RD1654936992988','厨房','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg',''),(660000009,'RPD1655802429730','RD1654152157975','厨房','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg',''),(660000010,'RPD1655802878517','RD1655802627140','客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg',''),(660000011,'RPD1655802882166','RD1655802627140','厨房','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg',''),(660000012,'RPD1655802888315','RD1655802724886','客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg',''),(660000013,'RPD1655802891420','RD1655802724886','厨房','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg',''),(660000014,'RPD1655802896735','RD1655802749958','客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg',''),(660000015,'RPD1655802899990','RD1655802749958','厨房','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg',''),(660000016,'RPD1655802905648','RD1655802837265','客厅','http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg',''),(660000017,'RPD1655802908802','RD1655802837265','厨房','http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg','');
/*!40000 ALTER TABLE `roomplacelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0' COMMENT '用户账号状态 0正常 1禁用',
  `identity` varchar(255) NOT NULL DEFAULT '1' COMMENT '用户身份 0管理员 1用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin@2022.jyf','89b90b801816e09cc7ab68820d54fb28','0','0'),(6,'UID1651218095985','test26','d30ceacf031d9d9d2764d726d8ad1652','0','1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wxuser`
--

DROP TABLE IF EXISTS `wxuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wxuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `openid` longtext,
  `avatarUrl` varchar(255) DEFAULT NULL,
  `nickName` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0' COMMENT '默认0',
  `commonPlace` varchar(255) DEFAULT '厦门' COMMENT '用户常用地方，用于搜索索引等操作',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wxuser`
--

LOCK TABLES `wxuser` WRITE;
/*!40000 ALTER TABLE `wxuser` DISABLE KEYS */;
INSERT INTO `wxuser` VALUES (4,'UID1654158814966','onYN65JRB9vNp101pMoIG8GCG6Vs','https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIamF3jz3e9htITltxBPiau218zSTgCjXQWQEYZY70xW0WYDX1GXQxN5QAsynyHomQ2qEh6VZTmlOg/132','_WaCz','nullGender','0','厦门');
/*!40000 ALTER TABLE `wxuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'wx_house_pro'
--

--
-- Dumping routines for database 'wx_house_pro'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-21 17:17:10
