/*
 Navicat Premium Data Transfer

 Source Server         : HMS
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : wx_house_production

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 13/07/2022 22:38:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `houseId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1210000008 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES (1210000001, 'UID1651743691600', 'HD165178800001', '0');
INSERT INTO `collect` VALUES (1210000002, 'UID1651743691600', 'HD165178800002', '0');
INSERT INTO `collect` VALUES (1210000003, 'UID1651743691600', 'HD165178800003', '0');
INSERT INTO `collect` VALUES (1210000004, 'UID1654158814966', 'HD165178800003', '0');
INSERT INTO `collect` VALUES (1210000006, 'UID1654158814966', 'HD165178800001', '0');
INSERT INTO `collect` VALUES (1210000007, 'UID1656689983563', 'HD165178800003', '0');

-- ----------------------------
-- Table structure for facilitylist
-- ----------------------------
DROP TABLE IF EXISTS `facilitylist`;
CREATE TABLE `facilitylist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `facilityId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设施编码',
  `facilityName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设施名称',
  `facilityType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设施类型 包含：基础设施 basic、洗浴设施 bath、厨房设施 kitchen、室外设施 outdoor、安全设施 safe、其他服务 other',
  `facilityUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设施图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30000024 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of facilitylist
-- ----------------------------
INSERT INTO `facilitylist` VALUES (30000001, 'FD16830000001', '大床', 'basic', '../../icon/equip/dachuang.png');
INSERT INTO `facilitylist` VALUES (30000002, 'FD16830000002', '空调', 'basic', '../../icon/equip/kongtiao.png');
INSERT INTO `facilitylist` VALUES (30000003, 'FD16830000003', 'WIFI', 'basic', '../../icon/equip/wifi.png');
INSERT INTO `facilitylist` VALUES (30000004, 'FD16830000004', '暖气', 'basic', '../../icon/equip/nuanqi.png');
INSERT INTO `facilitylist` VALUES (30000005, 'FD16830000005', '沙发', 'basic', '../../icon/equip/shafa.png');
INSERT INTO `facilitylist` VALUES (30000006, 'FD16830000006', '餐桌', 'basic', '../../icon/equip/canzhuo.png');
INSERT INTO `facilitylist` VALUES (30000007, 'FD16830000007', '电梯', 'basic', '../../icon/equip/dianti.png');
INSERT INTO `facilitylist` VALUES (30000008, 'FD16830000008', '电视', 'basic', '../../icon/equip/dianshi.png');
INSERT INTO `facilitylist` VALUES (30000009, 'FD16830000009', '热水器', 'bath', '../../icon/equip/reshuiqi.png');
INSERT INTO `facilitylist` VALUES (30000010, 'FD16830000010', '洗衣机', 'bath', '../../icon/equip/xiyiji.png');
INSERT INTO `facilitylist` VALUES (30000011, 'FD16830000011', '卫生间', 'bath', '../../icon/equip/weishengjian.png');
INSERT INTO `facilitylist` VALUES (30000012, 'FD16830000012', '冰箱', 'kitchen', '../../icon/equip/bingxiang.png');
INSERT INTO `facilitylist` VALUES (30000013, 'FD16830000013', '油烟机', 'kitchen', '../../icon/equip/youyanji.png');
INSERT INTO `facilitylist` VALUES (30000014, 'FD16830000014', '微波炉', 'kitchen', '../../icon/equip/weibolu.png');
INSERT INTO `facilitylist` VALUES (30000015, 'FD16830000015', '健身房', 'outdoor', '../../icon/equip/jianshenfang.png');
INSERT INTO `facilitylist` VALUES (30000016, 'FD16830000016', '公交车', 'outdoor', '../../icon/equip/gongjiaoche.png');
INSERT INTO `facilitylist` VALUES (30000017, 'FD16830000017', '地铁', 'outdoor', '../../icon/equip/ditie.png');
INSERT INTO `facilitylist` VALUES (30000018, 'FD16830000018', '快递驿站', 'outdoor', '../../icon/equip/cainiaoyizhan.png');
INSERT INTO `facilitylist` VALUES (30000019, 'FD16830000019', '超市', 'outdoor', '../../icon/equip/chaoshi.png');
INSERT INTO `facilitylist` VALUES (30000020, 'FD16830000020', '充电桩', 'outdoor', '../../icon/equip/chongdianzhuang.png');
INSERT INTO `facilitylist` VALUES (30000021, 'FD16830000021', '烟雾报警', 'safe', '../../icon/equip/yanwubaojing.png');
INSERT INTO `facilitylist` VALUES (30000022, 'FD16830000022', '消防栓', 'safe', '../../icon/equip/xiaofangshuan.png');
INSERT INTO `facilitylist` VALUES (30000023, 'FD16830000023', '可开发票', 'other', '../../icon/equip/yes.png');

-- ----------------------------
-- Table structure for houselist
-- ----------------------------
DROP TABLE IF EXISTS `houselist`;
CREATE TABLE `houselist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `houseId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓编码',
  `houseName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓名称',
  `houseLongName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓长名称',
  `houseLongitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓经度',
  `houseLatitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓纬度',
  `housePlace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓地址',
  `landlordId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房东编码 用房东表选择',
  `houseCity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓省市（暂时没用到）后续推广后增加根据区域推荐优秀房源',
  `advantageList` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '优点列表',
  `houseTypeListId` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '拥有房型',
  `lessPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '最低价格',
  `houseCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓房码',
  `lessRentType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最低押金',
  `houseMainUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间主图片，用来热门推荐和收藏时显示',
  `isHot` tinyint(1) NULL DEFAULT 0 COMMENT '布尔类型 是否热门',
  `houseOwnType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '2022-06-01新增属性：公寓拥有类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8800007 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of houselist
-- ----------------------------
INSERT INTO `houselist` VALUES (8800001, 'HD165178800001', '舒鑫佳苑', '《 舒鑫佳苑 》 轻奢大房间 北欧ins风格 ', '118.13805', '24.523646', '福建省厦门市湖里区林后社555号林后小区东门', 'LD1651743698686', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 1650.00, '8001', '押一付一', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 0, 'LOFT,两室一厅');
INSERT INTO `houselist` VALUES (8800002, 'HD165178800002', '百顺公寓', '《 百顺公寓 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.144585', '24.529944', '福建省厦门市湖里区围里社1099号楼', 'LD1651743698687', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 1350.00, '8002', '押一付一', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 0, 'LOFT,一室一厅');
INSERT INTO `houselist` VALUES (8800003, 'HD165178800003', '永建顶尚', '《 永建顶尚 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.150131', '24.51308', '福建省厦门市湖里区枋湖南路185号', 'LD1651743698688', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 1300.00, '8003', '押一付三', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 1, '大单间,loft');
INSERT INTO `houselist` VALUES (8800004, 'HD165178800004', '闲寓·筱筑', '《 闲寓·筱筑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.150612', '24.524498', '福建省厦门市湖里区县后社391号', 'LD1651743698689', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 2100.00, '8004', '押二付一', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 1, '两室一厅,三室一厅');

-- ----------------------------
-- Table structure for houselist_backups
-- ----------------------------
DROP TABLE IF EXISTS `houselist_backups`;
CREATE TABLE `houselist_backups`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `houseId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓编码',
  `houseName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓名称',
  `houseLongName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓长名称',
  `houseLongitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓经度',
  `houseLatitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓纬度',
  `housePlace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓地址',
  `landlordId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房东编码 用房东表选择',
  `houseCity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓省市（暂时没用到）后续推广后增加根据区域推荐优秀房源',
  `advantageList` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '优点列表',
  `houseTypeListId` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '拥有房型',
  `lessPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '最低价格',
  `houseCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓房码',
  `lessRentType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最低押金',
  `houseMainUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间主图片，用来热门推荐和收藏时显示',
  `isHot` tinyint(1) NULL DEFAULT 0 COMMENT '布尔类型 是否热门',
  `houseOwnType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '2022-06-01新增属性：公寓拥有类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8800007 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of houselist_backups
-- ----------------------------
INSERT INTO `houselist_backups` VALUES (8800001, 'HD165178800001', '舒鑫佳苑', '《 舒鑫佳苑 》 轻奢大房间 北欧ins风格 ', '118.13805', '24.523646', '福建省厦门市湖里区林后社555号林后小区东门', 'LD1651743698686', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 1650.00, '8001', '押一付一', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 0, 'LOFT,两室一厅');
INSERT INTO `houselist_backups` VALUES (8800002, 'HD165178800002', '百顺公寓', '《 百顺公寓 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.144585', '24.529944', '福建省厦门市湖里区围里社1099号楼', 'LD1651743698687', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 1350.00, '8002', '押一付一', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 0, 'LOFT,一室一厅');
INSERT INTO `houselist_backups` VALUES (8800003, 'HD165178800003', '永建顶尚', '《 永建顶尚 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.150131', '24.51308', '福建省厦门市湖里区枋湖南路185号', 'LD1651743698688', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 1300.00, '8003', '押一付三', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 1, '大单间,loft');
INSERT INTO `houselist_backups` VALUES (8800004, 'HD165178800004', '闲寓·筱筑', '《 闲寓·筱筑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.150612', '24.524498', '福建省厦门市湖里区县后社391号', 'LD1651743698689', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, 2100.00, '8004', '押二付一', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', 1, '两室一厅,三室一厅');
INSERT INTO `houselist_backups` VALUES (8800005, 'HD1654155649604', '闲寓·筱筑15', '《 闲寓·筱筑 》 轻奢大房间 北欧ins风格 靠近板上地铁站 公交站相邻200米 楼下便利店超市菜鸟驿站', '118.150612', '24.524498', '福建省厦门市湖里区县后社391号', 'LD1651743698689', '福建省-厦门市-湖里区', '电子门锁，隔音好好，领包入住，免物业，靠近地铁，相邻超市，可做饭', NULL, NULL, '8005', '押二付一', 'https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large', 0, NULL);
INSERT INTO `houselist_backups` VALUES (8800006, 'HD1654929764978', '测试新增公寓', '测试新增公寓长标题', '118.13805', '24.523646', '福建福州', 'LD1651743698686', '福建省-厦门市-湖里区', '[\'XXXX\',\'XXXX\']', NULL, 1350.00, '8006', '押一付一', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', 0, 'LOFT,LOFT');

-- ----------------------------
-- Table structure for imglist
-- ----------------------------
DROP TABLE IF EXISTS `imglist`;
CREATE TABLE `imglist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `imgId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间图片编码',
  `imgUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `imgName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片名称',
  `imgRoomName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片房间名称',
  `imgType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片类型 包含1：3D图、2：主图片、3：图标图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 600007 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of imglist
-- ----------------------------
INSERT INTO `imglist` VALUES (600001, 'ID16496657006', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', 'XX公寓一室一厅客厅图片', '客厅', '1');
INSERT INTO `imglist` VALUES (600002, 'ID16496657007', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', 'XX公寓一室一厅厨房图片', '厨房', '1');
INSERT INTO `imglist` VALUES (600003, 'ID16496657008', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', '永建顶尚一室一厅主图片', '', '2');
INSERT INTO `imglist` VALUES (600004, 'ID16496657009', 'https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large', '永建顶尚单间主图片', NULL, '2');
INSERT INTO `imglist` VALUES (600005, 'ID1656514693200', 'http://110.42.186.39:3030/public/upload/images/0mm6ltn69yCWAK-vG3RrbQYf.png', '房东入住图片', '房东入住图片', '3');
INSERT INTO `imglist` VALUES (600006, 'ID1656514722518', 'http://110.42.186.39:3030/public/upload/images/oaGBjOT4QlaXUY7eFkxQ1Z93.png', '学生优惠图片', '学生优惠图片', '3');

-- ----------------------------
-- Table structure for landlorduser
-- ----------------------------
DROP TABLE IF EXISTS `landlorduser`;
CREATE TABLE `landlorduser`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `landlordId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房东编码',
  `landlordName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房东名称',
  `landlordPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房东电话',
  `landlordDetail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房东描述',
  `landlordPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '222666' COMMENT '房东密码 默认222666',
  `landlordType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '0新房东，1 超赞房东，2 实惠房东',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1004 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of landlorduser
-- ----------------------------
INSERT INTO `landlorduser` VALUES (1000, 'LD1651743698686', '廖房东', '15259788888', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');
INSERT INTO `landlorduser` VALUES (1001, 'LD1651743698687', '陈房东', '15259888886', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');
INSERT INTO `landlorduser` VALUES (1002, 'LD1651743698688', '李华容', '15486798686', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');
INSERT INTO `landlorduser` VALUES (1003, 'LD1651743698689', '龙化叶', '15486896622', '超赞房东经验丰富，致力于为房客提供优质的住宿体验。', '222666', '0');

-- ----------------------------
-- Table structure for roomimgslist
-- ----------------------------
DROP TABLE IF EXISTS `roomimgslist`;
CREATE TABLE `roomimgslist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `roomId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `imgId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isExit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `houseId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomimgslist
-- ----------------------------
INSERT INTO `roomimgslist` VALUES (1, 'RD16868000001', 'ID16496657009', '0', 'HD165178800001');

-- ----------------------------
-- Table structure for roomlist
-- ----------------------------
DROP TABLE IF EXISTS `roomlist`;
CREATE TABLE `roomlist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `roomId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间编码',
  `houseId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓编码',
  `facilityList` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '设施列表',
  `roomRent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公寓租金',
  `roomRentType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间押金',
  `roomList` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间空间图片 暂时不用',
  `roomTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间标题',
  `roomType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间类型',
  `roomMainUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间主图片',
  `mark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `onlyImg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否仅为图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 800000010 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomlist
-- ----------------------------
INSERT INTO `roomlist` VALUES (680000001, 'RD16868000001', 'HD165178800001', '[\"FD16830000001\",\"FD16830000002\",\"FD16830000003\",\"FD16830000004\",\"FD16830000005\",\"FD16830000006\"]', '1650', '押一付一', '暂不填', '测试舒鑫佳苑LOFT', 'LOFT', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', NULL, '1');
INSERT INTO `roomlist` VALUES (800000002, 'RD16868000002', 'HD165178800003', '[\"FD16830000001\",\"FD16830000002\"]', '1300', '押一付一', NULL, '永建顶尚 大单间 （位于1、4和6层）', '大单间', 'https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large', NULL, '0');
INSERT INTO `roomlist` VALUES (800000003, 'RD1654152157975', 'HD165178800003', '[\"FD16830000001\",\"FD16830000002\",\"FD16830000003\",\"FD16830000004\",\"FD16830000005\"]', '1450', '押一付一', NULL, '永建顶尚 loft （位于8和12层）', 'loft', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', NULL, '0');
INSERT INTO `roomlist` VALUES (800000005, 'RD1654936992988', 'HD165178800004', '[\"FD16830000001\",\"FD16830000002\",\"FD16830000004\"]', '2100', '押一付一', NULL, '测试闲寓两室一厅', '两室一厅', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', NULL, '0');
INSERT INTO `roomlist` VALUES (800000006, 'RD1655802627140', 'HD165178800001', '[\"FD16830000001\",\"FD16830000004\",\"FD16830000002\",\"FD16830000003\",\"FD16830000005\",\"FD16830000006\"]', '1850', '押一付一', NULL, '测试舒鑫佳苑两室一厅', '两室一厅', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', NULL, '0');
INSERT INTO `roomlist` VALUES (800000007, 'RD1655802724886', 'HD165178800002', '[\"FD16830000001\",\"FD16830000002\",\"FD16830000003\",\"FD16830000004\",\"FD16830000005\",\"FD16830000006\"]', '1350', '押一付一', NULL, '测试百顺公寓LOFT', 'LOFT', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', NULL, '0');
INSERT INTO `roomlist` VALUES (800000008, 'RD1655802749958', 'HD165178800002', '[\"FD16830000011\",\"FD16830000013\",\"FD16830000002\",\"FD16830000001\",\"FD16830000005\"]', '1450', '押一付一', NULL, '测试百顺公寓一室一厅', '一室一厅', 'https://z1.muscache.cn/im/pictures/7710c5cd-5198-40b4-8fd0-550f7b55919b.jpg?aki_policy=large', NULL, '0');
INSERT INTO `roomlist` VALUES (800000009, 'RD1655802837265', 'HD165178800004', '[\"FD16830000002\",\"FD16830000007\",\"FD16830000004\",\"FD16830000003\",\"FD16830000001\",\"FD16830000005\",\"FD16830000006\",\"FD16830000008\"]', '3000', '押一付一', NULL, '测试闲寓三室一厅', '三室一厅', 'https://z1.muscache.cn/im/pictures/351db42c-b302-40ea-bc5f-ac5f40bbe604.jpg?aki_policy=large', NULL, '0');

-- ----------------------------
-- Table structure for roomplacelist
-- ----------------------------
DROP TABLE IF EXISTS `roomplacelist`;
CREATE TABLE `roomplacelist`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `roomPlaceId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间具体地方编码',
  `roomId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间编码',
  `roomPlaceName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间具体地方名称 例如客厅 厨房等',
  `roomPlaceImg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '房间具体地方图片',
  `mark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 660000018 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomplacelist
-- ----------------------------
INSERT INTO `roomplacelist` VALUES (660000001, 'RPD16866000001', 'RD16868000001', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '永建顶尚 一室一厅 客厅展示');
INSERT INTO `roomplacelist` VALUES (660000002, 'RPD16866000002', 'RD16868000001', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '永建顶尚 一室一厅 厨房展示');
INSERT INTO `roomplacelist` VALUES (660000003, 'RPD1654940017483', 'RD1654936992988', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '测试客厅');
INSERT INTO `roomplacelist` VALUES (660000005, 'RPD1655733567310', 'RD16868000002', '房间客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '房间客厅');
INSERT INTO `roomplacelist` VALUES (660000006, 'RPD1655733576091', 'RD16868000002', '房间厨房', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '房间厨房');
INSERT INTO `roomplacelist` VALUES (660000007, 'RPD1655802406583', 'RD1654152157975', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000008, 'RPD1655802418313', 'RD1654936992988', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000009, 'RPD1655802429730', 'RD1654152157975', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000010, 'RPD1655802878517', 'RD1655802627140', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000011, 'RPD1655802882166', 'RD1655802627140', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000012, 'RPD1655802888315', 'RD1655802724886', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000013, 'RPD1655802891420', 'RD1655802724886', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000014, 'RPD1655802896735', 'RD1655802749958', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000015, 'RPD1655802899990', 'RD1655802749958', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000016, 'RPD1655802905648', 'RD1655802837265', '客厅', 'http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg', '');
INSERT INTO `roomplacelist` VALUES (660000017, 'RPD1655802908802', 'RD1655802837265', '厨房', 'http://110.42.186.39:3030/public/upload/images/72N-5iZuMb3hRns90xio2MWq.jpg', '');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '用户账号状态 0正常 1禁用',
  `identity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '用户身份 0管理员 1用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin@2022.jyf', '89b90b801816e09cc7ab68820d54fb28', '0', '0');
INSERT INTO `user` VALUES (6, 'UID1651218095985', 'test26', 'd30ceacf031d9d9d2764d726d8ad1652', '0', '1');

-- ----------------------------
-- Table structure for wxuser
-- ----------------------------
DROP TABLE IF EXISTS `wxuser`;
CREATE TABLE `wxuser`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `openid` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `avatarUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nickName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '默认0',
  `commonPlace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '厦门' COMMENT '用户常用地方，用于搜索索引等操作',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wxuser
-- ----------------------------
INSERT INTO `wxuser` VALUES (4, 'UID1654158814966', 'onYN65JRB9vNp101pMoIG8GCG6Vs', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIamF3jz3e9htITltxBPiau218zSTgCjXQWQEYZY70xW0WYDX1GXQxN5QAsynyHomQ2qEh6VZTmlOg/132', 'WaCz', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (5, 'UID1656431961677', 'onYN65HnjVwzaTjz4ougNGTmWtUw', 'https://thirdwx.qlogo.cn/mmopen/vi_32/nHTeBrP98wwRVibhNHwMwsASop8wIic0aLTlaPR3BEEGh35QzAEWWq7oSjribVjBvNbC3toYKZ6DQQUcnkJy2XnIw/132', '福星高照', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (6, 'UID1656472381968', 'onYN65Aia7WD2bxDcMR6aaDOU-i4', 'https://thirdwx.qlogo.cn/mmopen/vi_32/tro5ojiaUYicMQ4JNficuorTMOEdVLPVWWn8TJR5zeJkLCsqLyJXn4gYICZoXdNK9gZTyR2ibe3Gic3QMQicxZGXD3ibA/132', '戟画', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (7, 'UID1656503026362', 'onYN65GwRQttFjih4zzKuwmPPijk', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PKaDFjWhFRkUsfGfUwNUJhnVHq8s6cyXwc2Lc75LXGPFoY4xF66otlvSGvn820icQbFmkwmCHR2C1WoeQjyN38w/132', '途寓', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (8, 'UID1656556603898', 'onYN65IGDZ2sfC7LaSyQFbbSAzlY', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLLQSLw8OnywtbZ86LGELwR1T7LvvkGv9hLEaoVeiaX791mTDJ5SVqWKC1CfbeHdrlibEjcVMVFYb4A/132', '猫毛', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (9, 'UID1656557006469', 'onYN65OkO1qtHaQRuXQHTeeOSA0U', 'https://thirdwx.qlogo.cn/mmopen/vi_32/666ZxTwTYHVSlvyCJiaNhcr56Z1bwLaQ0NeOt0nj2SZPtUpjY1DXCLia0gUnaTvWykLBrtzomGASpzVClJWyp6rA/132', '小倩', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (10, 'UID1656568129505', 'onYN65Fl_Z6l3fF-7MGzygvoYj00', 'https://thirdwx.qlogo.cn/mmopen/vi_32/8XPTZty0gTBastaym2MDNxGDrS5mrbtUPF6lnqHLWeCqCrYOml7nevO5k9RhfWQGKQ77Q9vdkw3FYhCpvxcDSA/132', 'glen519', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (11, 'UID1656586678840', 'onYN65HbnmsWh3VwlbS10XHWII6g', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTK44yGr1ddyicNJudD7Qeg99nNtdasA8jOOeJ4Gj4eagcadPmic83xzQAFIgr9jnOrpVjPkhmvreV2w/132', '', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (12, 'UID1656660537142', 'onYN65EoAc2wCgtY5XGOq_Ppr_U8', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ep8IcW9Wgeuib9oY6xzkoRQ4yBich1iav9vW5uDykFF0zJ6cVyU7HATDdqIveaETosFRSAM9ic7BVlBtA/132', '杨陈浩彤', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (13, 'UID1656689983563', 'onYN65K21k8lYLF7Y6OSUkphngyw', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJadZq0YcHmNJDddKSXtWEXVyveeIoALexKKFgHvFh472NT67uzULVd92xEMEIIehuvPrC3xT5ufA/132', '英阿。', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (14, 'UID1656746165492', 'onYN65FvJkM4vEfdb4CsBHTAYzUQ', 'https://thirdwx.qlogo.cn/mmopen/vi_32/nqb5xRShV6ibzibDCem9x6VbrWXIyUdZO2stQaRYjfibyXE6oCEk9IrqGg02r0kJHBfEw2Bz7fwYb07Fdch2xRRFw/132', '低调', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (15, 'UID1656834013133', 'onYN65HuVvxv6aWNppUNPn3K2c-8', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erlQmGfZhIWapVrdq34Sx5AtBM5Ob9LaJ8tyH82shicuQ6FBAU7VQZEApb48icJPVEdvN4lV2ibcHeiaw/132', 'Wang', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (16, 'UID1656860875458', 'onYN65JAQPM0yzhHiU8UuwjWMoy8', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLWriaUvxHqWIVfdkv74iaib5N6cCPQf6GYibdKwRpnFmibojeWyPWteLrHpvBibSZcc4Nr5nYqCc8z8UNg/132', 'H', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (17, 'UID1657029551981', 'onYN65DivC16MgVE1qL0gAd3x_tM', 'https://thirdwx.qlogo.cn/mmopen/vi_32/hbuIPqNDic0xoqDSicbqGeDIPTgNpLm5ibhXYNZ6ticUib8ibwtGqa1Y3RsHZwMDDrUicu5NgS3Cxt6ibYZwe45au9HQyw/132', '一见如故', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (18, 'UID1657107247169', 'onYN65B0degMvhslMckaiEe3jS_w', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ervV3bVelpRMadDQcAWxwCzPuJshYptMoxqOQJeXK3FsN2TWOmsicSeS1Ay68sMfXAWibLNqAVFKYPw/132', 'coco', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (19, 'UID1657167798095', 'onYN65GVgt_1MHSP2WvBOei5X1uI', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIX30nQ8op79KoR4fXNzSdOeaHPl7pCcQHWTYneDc3RP8icDFCAJyaJiajTMnjPt1ZknZIibcMamRc6A/132', 'Victoria 王翔宇', 'nullGender', '0', '厦门');
INSERT INTO `wxuser` VALUES (20, 'UID1657431667637', 'onYN65E79an1cCY1gvnn3h6cPYpY', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqrV6wsJ1K5Nf6aEKYctLHn4W7DmfTcZLdCFbPzaohE28CM5KMRJdk0g8TqCb30sSlWgn39j3l5lw/132', '650', 'nullGender', '0', '厦门');

SET FOREIGN_KEY_CHECKS = 1;
