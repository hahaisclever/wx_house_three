var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");

// 1. 用户查看收藏列表 (二)
router.post("/getUserCollect", async (req, res) => {
    const { uid } = req.body;
    let sqlStr = `
    SELECT houselist.houseId, houselist.houseName, houselist.houseMainUrl, houselist.lessPrice, landlorduser.landlordPhone, houselist.housePlace, houselist.lessRentType FROM collect
    INNER JOIN houselist ON collect.houseId = houselist.houseId
    INNER JOIN landlorduser ON houselist.landlordId = landlorduser.landlordId
    WHERE uid=?
    `;
    let result = await sqlQuery(sqlStr, [uid]);
    res.send({
        code: result.length >= 0 ? 200 : 400,
        message: result.length >= 0 ? "用户收藏列表搜索成功" : "用户暂无收藏列表",
        data: result
    });
});

// 2. 用户添加房间到收藏列表 (七)
router.post("/addUserCollect", async (req, res) => {
    const { uid, houseId } = req.body;
    let sqlStr = `INSERT INTO collect(uid, houseId) VALUES(?,?)`;
    let result = await sqlQuery(sqlStr, [uid, houseId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "用户添加收藏列表成功" : "用户添加收藏列表失败"
    });
});

// 3. 用户取消收藏 (八)
router.post("/deleteUserCollect", async (req, res) => {
    const { uid,houseId } = req.body;
    let sqlStr = "delete from collect where uid=? and houseId=?";
    let result = await sqlQuery(sqlStr, [uid,houseId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "用户取消收藏成功" : "用户取消收藏失败"
    });
});

// 4. 房东入驻滚动条 (九)
router.post("/getLandlordJoinInfo", async (req, res) => {
    let sqlStr = `SELECT houselist.houseId, houselist.houseName, landlorduser.landlordName FROM houselist
INNER JOIN landlorduser ON houselist.landlordId = landlorduser.landlordId
order by houselist.id DESC limit 5`;
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length >= 0 ? 200 : 400,
        message: result.length >= 0 ? "房东入驻滚动条信息获取成功" : "房东入驻滚动条信息获取失败",
        data: result
    });
});

// 5. 选房搜索房码 类似查看公寓接口 (六)
router.post("/searchHouseByHouseCode", async (req, res) => {
    const { houseCode, uid } = req.body;
    let sqlStr = "SELECT * FROM houselist WHERE houseCode=?"; // 获取公寓信息
    let result = await sqlQuery(sqlStr, [houseCode]);
    if (result.length > 0) {
        let sqlStrRooms = "SELECT roomId, roomType, onlyImg FROM roomlist WHERE houseId=?"; // 获取房间列表
        let resultRooms = await sqlQuery(sqlStrRooms, [result[0].houseId]);
        let sqlStrLandlord = "SELECT * FROM landlorduser WHERE landlordId=?"; // 获取房东信息
        let resultLandlord = await sqlQuery(sqlStrLandlord, [result[0].landlordId]);
        let sqlStrCollect = "SELECT * FROM collect WHERE uid=? AND houseId=?"; // 查找用户是否收藏该公寓
        let resultCollect = await sqlQuery(sqlStrCollect, [uid, result[0].houseId]);
        let sqlStrImageList =  `SELECT roomimgslist.imgId, roomimgslist.roomId, roomimgslist.isExit, imglist.imgUrl, imglist.imgName, imglist.imgRoomName, imglist.imgType FROM roomimgslist INNER JOIN imglist ON roomimgslist.imgId = imglist.imgId WHERE roomimgslist.houseId =?`;
        let resultImages = await sqlQuery(sqlStrImageList, [result[0].houseId]);
        const houseInfo = {
            ...result[0],
            roomsList: resultRooms,
            roomImageList: resultImages,
            landlordInfo: resultLandlord[0],
            isCollect: resultCollect[0] && resultCollect[0].status == "0" ? true : false
        };
        res.send({
            code: 200,
            message: "通过房码搜索公寓成功",
            data: houseInfo
        });
    } else {
        res.send({
            code: 400,
            message: "该房码暂无匹配公寓"
        });
    }
});

// 6 通过公寓id搜索公寓信息 (六)
router.post("/searchHouseByHouseId", async (req, res) => {
    const { houseId, uid } = req.body;
    let sqlStr = "SELECT * FROM houselist WHERE houseId=?"; // 获取公寓信息
    let result = await sqlQuery(sqlStr, [houseId]);
    if (result.length > 0) {
        let sqlStrRooms = "SELECT roomId, roomType FROM roomlist WHERE houseId=?"; // 获取房间列表
        let resultRooms = await sqlQuery(sqlStrRooms, [result[0].houseId]);
        let sqlStrLandlord = "SELECT * FROM landlorduser WHERE landlordId=?"; // 获取房东信息
        let resultLandlord = await sqlQuery(sqlStrLandlord, [result[0].landlordId]);
        let sqlStrCollect = "SELECT * FROM collect WHERE uid=? AND houseId=?"; // 查找用户是否收藏该公寓
        let resultCollect = await sqlQuery(sqlStrCollect, [uid, result[0].houseId]);
        let sqlStrImageList =  `SELECT roomimgslist.imgId, roomimgslist.roomId, roomimgslist.isExit, imglist.imgUrl, imglist.imgName, imglist.imgRoomName, imglist.imgType FROM roomimgslist INNER JOIN imglist ON roomimgslist.imgId = imglist.imgId WHERE roomimgslist.houseId =?`;
        let resultImages = await sqlQuery(sqlStrImageList, [result[0].houseId]);
        const houseInfo = {
            ...result[0],
            roomsList: resultRooms,
            roomImageList: resultImages,
            landlordInfo: resultLandlord[0],
            isCollect: resultCollect[0] && resultCollect[0].status == "0" ? true : false
        };
        res.send({
            code: 200,
            message: "通过房码搜索公寓成功",
            data: houseInfo
        });
    } else {
        res.send({
            code: 400,
            message: "该房码暂无匹配公寓"
        });
    }
});

// 7 进入热门页面，自动搜索相关区域房间和热门推荐，相当于直接根据搜索框旁地址进行热门搜索和普通5条相关地区公寓 (四)
router.post("/searchHotAndAllHouseList", async (req, res) => {
    const { houseCity } = req.body;
    let sqlStr = `SELECT * FROM houselist WHERE houseCity LIKE "%${houseCity}%" limit 5`; // 获取公寓信息
    let result = await sqlQuery(sqlStr);
    let sqlStrHot = `SELECT * FROM houselist WHERE houseCity LIKE "%${houseCity}%" AND isHot = '1'`;
    let resultHot = await sqlQuery(sqlStrHot, [houseCity]);
    res.send({
        code: 200,
        message: "进入搜索页接口调用成功",
        data: {
            hotHouseList: resultHot,
            searchHouseList: result
        }
    });
});

// 8. 热门好房推荐  (五)
/**如只做厦门地区，则不用地区参数。后续要做地区分销。则在用户处需要保存用户地址，用地址搜索热门公寓 */
router.post("/hotHouseRecommend", async (req, res) => {
    const { houseCity } = req.body;
    let sqlStr = `SELECT * FROM houselist WHERE houseCity LIKE "%${houseCity}%" AND isHot = '1'`;
    let result = await sqlQuery(sqlStr, [houseCity]);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "热门好房推荐搜索成功" : "热门好房推荐搜索失败",
        data: result
    });
});

// 9. 选房搜索住宅名称 (四)
router.post("/searchHouseByHouseName", async (req, res) => {
    const { houseName, houseCity } = req.body;
    let sqlStr = `SELECT * FROM houselist WHERE houseName LIKE "%${houseName}%" AND houseCity LIKE "%${houseCity}%" limit 5`; // 获取公寓信息
    let result = await sqlQuery(sqlStr);
    if (result.length > 0) {
        res.send({
            code: 200,
            data: result,
            message: "公寓搜索成功"
        });
    } else {
        res.send({
            code: 400,
            data: [],
            message: "该房码暂无匹配公寓"
        });
    }
});

// 10. 查看房间详情接口 (十)
router.post("/getHouseRoomDetail", async (req, res) => {
    const { houseId, roomId } = req.body;
    let sqlStr = `SELECT * FROM roomlist WHERE houseId = ? AND roomId = ?`; // 获取公寓信息
    let result = await sqlQuery(sqlStr, [houseId, roomId]);
    if (result.length > 0) {
        // 搜索全部的设备列表
        let sqlStrFacility = `SELECT * FROM facilitylist`; // 获取公寓信息
        let facilityList = await sqlQuery(sqlStrFacility);
        let sqlStrPlace = `SELECT * FROM roomplacelist WHERE roomId = ?`;
        let placeList = await sqlQuery(sqlStrPlace, [roomId]);
        if (!placeList || placeList.length <= 0) {
            res.send({
                code: 400,
                message: "房间详细列表图搜索失败"
            });
            return;
        }
        const ownFacilityList = [];
        if (facilityList && facilityList.length) {
            const facilityIdList = JSON.parse(result[0].facilityList);
            facilityList.map((item) => {
                if (facilityIdList.includes(item.facilityId)) {
                    ownFacilityList.push(item);
                }
            });
        }
        res.send({
            code: 200,
            data: {
                ...result[0],
                facilityList: ownFacilityList,
                placeList
            },
            message: "房间详细搜索成功"
        });
    } else {
        res.send({
            code: 400,
            message: "房间详细搜索失败，没找到匹配的公寓id对应房间id"
        });
    }
});

// 11. 查看房间详情接口 (十一)
router.post("/searchMapLoaction", async (req, res) => {
    let sqlStr = `SELECT id, houseId, houseName, houseLongitude, houseLatitude FROM houseList`; // 获取公寓地图坐标列表
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "获取公寓地图坐标列表成功" : "获取公寓地图坐标列表失败",
        data: result
    });
});

// 12. 查看房东其他房源接口 (十二)
router.post("/getLandlordAllHouseList", async (req, res) => {
    const { landlordId } = req.body;
    let sqlStr = `SELECT id, houseId, houseName, housePlace, houseOwnType, houseMainUrl FROM houseList WHERE landlordId = ?`; // 获取公寓地图坐标列表
    let result = await sqlQuery(sqlStr, [landlordId]);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "获取房东其他房源列表成功" : "获取房东其他房源列表失败",
        data: result
    });
});

// 13. 新增根据houseCode判断是否进行页面跳转
router.post("/isHouseCodeExit", async (req, res) => {
    const { houseCode } = req.body;
    let sqlStr = `SELECT id FROM houseList WHERE houseCode=?`; // 获取公寓地图坐标列表
    let result = await sqlQuery(sqlStr,[houseCode]);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "房码查找成功" : "房码查找失败",
    });
});
module.exports = router;
