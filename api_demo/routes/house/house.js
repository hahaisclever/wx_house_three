var express = require("express");
var router = express.Router();
var fs = require("fs");
// 引入数据库模块
const sqlQuery = require("../../model/mysql");
const multiparty = require("multiparty"); //获取上传的图片功能

// !!! 上传和删除的图片的路径都需要抽离出来，防止上传服务器后不生效问题

// const imagesUrlLocal = "http://127.0.0.1:3000/";
const imagesUrlLocal = "http://110.42.186.39:3030/";

/** 删除本地文件
 * @param { delPath：String } （需要删除文件的地址）
 */

// 1. 获取图片列表
router.post("/getHouseImgList", async (req, res) => {
    let sqlStr = "select * from imglist";
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "图片列表搜索成功" : "图片列表搜索失败",
        data: result
    });
});

// 2. 上传图片 并保存路径到数据库中
router.post("/uploadHouseImg", (req, res) => {
    var form = new multiparty.Form();
    const imgId = "ID" + Date.now();
    form.uploadDir = "public/upload/images";
    //上传图片保存的地址(目录必须存在)
    form.parse(req, async (err, fields, files) => {
        // 1、fields:获取表单的数据 2、files：图片上传成功返回的信息
        var url = imagesUrlLocal + files.file[0].path;
        let addSql = "INSERT INTO imglist(imgId,imgUrl) VALUES(?,?)";
        let result = await sqlQuery(addSql, [imgId, url]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "图片添加成功" : "图片添加失败"
        });
    });
});

// 3. 删除数据库数据，并删除本地图片
router.post("/deleteHouseImg", async (req, res) => {
    const { imgId, imgUrl } = req.body;
    const delPath = "./public/upload/images/" + imgUrl;
    try {
        /**
         * @des 判断文件或文件夹是否存在
         */
        if (fs.existsSync(delPath)) {
            fs.unlinkSync(delPath);
            let sqlStr = "delete from imglist where imgId=?";
            let result = await sqlQuery(sqlStr, [imgId]);
            res.send({
                code: result.affectedRows >= 1 ? 200 : 400,
                message: result.affectedRows >= 1 ? "图片删除成功" : "图片删除失败"
            });
        } else {
            res.send({
                code: 400,
                message: delPath + "该文件没找到"
            });
        }
    } catch (error) {
        res.send({
            code: 400,
            message: "del error" + error
        });
    }
});

// 4. 获取房间列表
router.post("/getTipsList", async (req, res) => {
    let sqlStr = "select * from tipsList";
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "房间列表搜索成功" : "房间列表搜索失败",
        data: result
    });
});

// 5. 创建新的房间列表
router.post("/addNewTips", async (req, res) => {
    const { room_name } = req.body;
    let isOnlyStr = "select * from tipslist where room_name=?";
    let onlyResult = await sqlQuery(isOnlyStr, [room_name]);
    if (onlyResult.length > 0) {
        res.send({
            code: 400,
            message: "房间名称重复"
        });
        return;
    }
    const tips_id = "tips" + Date.now();
    let sqlStr = "INSERT INTO tipslist(tips_id, room_name) VALUES(?,?)";
    let result = await sqlQuery(sqlStr, [tips_id, room_name]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "图片添加成功" : "图片添加失败"
    });
});

// 6. 删除房间列表
router.post("/deleteTipsList", async (req, res) => {
    const { tips_id } = req.body;
    let sqlStr = "delete from tipslist where tips_id=?";
    let sqlTipsStr = "delete from tips_pc_relation where tips_id=?";
    let result = await sqlQuery(sqlStr, [tips_id]);
    // 删除关联信息
    await sqlQuery(sqlTipsStr, [tips_id]);
    res.send({
        code: result.affectedRows >= 1 ? 200 : 400,
        message: result.affectedRows >= 1 ? "房间信息删除成功" : "房间信息删除失败"
    });
});

// 7. 获取房间点位列表
router.post("/getTipsPcList", async (req, res) => {
    const { tips_id } = req.body;
    let sqlStr = "SELECT * FROM tips_pc_relation INNER JOIN pclist ON tips_pc_relation.pc_id = pclist.pc_id WHERE tips_id=?";
    let result = await sqlQuery(sqlStr, [tips_id]);
    res.send({
        code: result.length >= 0 ? 200 : 400,
        message: result.length >= 0 ? "房间关联点位 搜索成功" : "房间关联点位 搜索失败",
        data: result
    });
});

// 8. 关联图片和房间
router.post("/relationTipsAndImage", async (req, res) => {
    const { imgId, tips_id } = req.body;
    let sqlClearStr = "UPDATE imglist SET tips_id=? WHERE tips_id=?"; // 先清空原本已经使用的
    await sqlQuery(sqlClearStr, [null, tips_id]);
    let sqlStr = "UPDATE imglist SET tips_id=? WHERE imgId=?";
    let result = await sqlQuery(sqlStr, [tips_id, imgId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "图片关联房间成功" : "图片关联房间失败"
    });
});

// 9. 房间明细表
router.post("/getTipsDetailList", async (req, res) => {
       const dataList = {
           image: "http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg",
           tipsList: []
       };
       res.send({
           code: 200,
           message: "房间明细搜索成功",
           data: dataList
       });

    // const { tips_id } = req.body;
    // let sqlStr = `
    // SELECT
    // tips_pc_relation.tips_id, tips_pc_relation.pc_id,
    // pclist.title, pclist.text,pclist.x, pclist.y, pclist.z, pclist.showTip, pclist.showTitle, pclist.image, pclist.id,
    // imglist.imgId, imglist.imgUrl
    // FROM tips_pc_relation
    // INNER JOIN pclist ON tips_pc_relation.pc_id = pclist.pc_id
    // INNER JOIN imglist ON tips_pc_relation.tips_id = imglist.tips_id
    // WHERE tips_pc_relation.tips_id =?
    // `;
    // let result = await sqlQuery(sqlStr, [tips_id]);
    // if (result.length>0){
    //     const dataList = {
    //         image: "http://110.42.186.39:3030/public/upload/images/olnX_dQZkdg0GTU725UEjZGX.jpg",
    //         tipsList: []
    //     };
        // result.map(item=>{
        //     dataList.tipsList.push({
        //         position: {
        //             x: item.x,
        //             y: item.y,
        //             z: item.z
        //         },
        //         content: {
        //             title: item.title,
        //             text: item.text,
        //             image: item.image,
        //             showTip: item.showTip,
        //             showTitle: item.showTitle
        //         }
        //     });
        // })
    //     res.send({
    //         code: 200,
    //         message: "房间明细搜索成功",
    //         data: dataList
    //     });
    // }else{
    //     res.send({
    //         code: 400,
    //         message: "房间明细搜索失败",
    //     });
    // }
});

module.exports = router;
