var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");

// 同时增改查两张表，主表houselist和备份表houselist_backups;备份表不做删除操作

// 1. 添加新公寓 houseCode房码唯一
router.post("/addHouseInfo", async (req, res) => {
    const houseId = "HD" + Date.now();
    const { houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, houseCode, lessRentType, houseMainUrl } = req.body;
    if (houseName && houseLongName && houseLongitude && houseLatitude && housePlace && landlordId && houseCity && advantageList && houseCode && lessRentType && houseMainUrl) {
        let onlySqlStr = "SELECT houseCode FROM houselist WHERE houseCode=?";
        let resultOnly = await sqlQuery(onlySqlStr, [houseCode]);
        if (resultOnly.length > 0) {
            res.send({
                code: 400,
                message: "房码已被使用"
            });
            return;
        }
        let sqlStr = "INSERT INTO houselist(houseId, houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, houseCode, lessRentType, houseMainUrl) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        let sqlStrBackUps = "INSERT INTO houselist_backups(houseId, houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, houseCode, lessRentType, houseMainUrl) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        let result = await sqlQuery(sqlStr, [houseId, houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, houseCode, lessRentType, houseMainUrl]);
        await sqlQuery(sqlStrBackUps, [houseId, houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, houseCode, lessRentType, houseMainUrl]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "添加新公寓成功" : "添加新公寓失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 2. 修改公寓信息 房码不支持修改
router.post("/editHouseInfo", async (req, res) => {
    const { houseId, houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, lessRentType, houseMainUrl } = req.body;
    if (houseId && houseName && houseLongName && houseLongitude && houseLatitude && housePlace && landlordId && houseCity && advantageList && lessRentType && houseMainUrl) {
        let sqlStr = "UPDATE houselist SET houseName=?, houseLongName=?, houseLongitude=?, houseLatitude=?, housePlace=?, landlordId=?, houseCity=?, advantageList=?, lessRentType=?, houseMainUrl=? WHERE houseId=?";
        let sqlStrBackUps = "UPDATE houselist_backups SET houseName=?, houseLongName=?, houseLongitude=?, houseLatitude=?, housePlace=?, landlordId=?, houseCity=?, advantageList=?, lessRentType=?, houseMainUrl=? WHERE houseId=?";
        let result = await sqlQuery(sqlStr, [houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, lessRentType, houseMainUrl, houseId]);
        await sqlQuery(sqlStrBackUps, [houseName, houseLongName, houseLongitude, houseLatitude, housePlace, landlordId, houseCity, advantageList, lessRentType, houseMainUrl, houseId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "修改新公寓成功" : "修改新公寓失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 3. 删除公寓信息（不推荐使用）
router.post("/deleteHouseInfo", async (req, res) => {
    const { houseId } = req.body;
    if (houseId) {
        let sqlStr = "DELETE FROM houselist WHERE houseId=?";
        let result = await sqlQuery(sqlStr, [houseId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "公寓信息删除成功" : "公寓信息删除失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 4. 查找公寓信息
router.post("/getHouseInfo", async (req, res) => {
    const { houseName } = req.body;
    let sqlStr = `SELECT * FROM houselist WHERE houseName LIKE "%${houseName}%"`;
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "查找公寓信息成功" : "查找公寓信息失败",
        data: result
    });
});

// 5. 设置公寓为热门公寓 isHot
router.post("/setHouseToHot", async (req, res) => {
    const { houseId } = req.body;
    let sqlStr = "UPDATE houselist SET isHot='1'  WHERE houseId=?";
    let sqlStrBackUps = "UPDATE houselist_backups SET isHot='1'  WHERE houseId=?";
    let result = await sqlQuery(sqlStr, [houseId]);
    await sqlQuery(sqlStrBackUps, [houseId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "公寓设置成热门成功" : "公寓设置成热门失败"
    });
});

// 6. 取消设置公寓为热门公寓 isHot
router.post("/cancelHouseToHot", async (req, res) => {
    const { houseId } = req.body;
    let sqlStr = "UPDATE houselist SET isHot='0'  WHERE houseId=?";
    let sqlStrBackUps = "UPDATE houselist_backups SET isHot='0'  WHERE houseId=?";
    let result = await sqlQuery(sqlStr, [houseId]);
    await sqlQuery(sqlStrBackUps, [houseId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "公寓取消设置成热门成功" : "公寓取消设置成热门失败"
    });
});

// 7. 一键同步公寓类型以及最低价 houseOwnType
router.post("/synclHouseInfo", async (req, res) => {
    const { houseId } = req.body;
    let sqlStr = "SELECT roomType, roomRent FROM roomList WHERE houseId=?";
    let result = await sqlQuery(sqlStr, [houseId]);
    // 处理公寓最低价和公寓类型
    if (result.length > 0) {
        let lessPriceCount = result[0].roomRent;
        let roomAllType = [];
        result.map((item) => {
            if (item.roomRent < lessPriceCount) {
                lessPriceCount = item.roomRent;
            }
            roomAllType.push(item.roomType);
        });
        let sqlStr = "UPDATE houselist SET lessPrice=?, houseOwnType=?  WHERE houseId=?";
        let sqlStrBackUps = "UPDATE houselist_backups SET lessPrice=?, houseOwnType=?  WHERE houseId=?";
        console.log(lessPriceCount, roomAllType.toString(), houseId);
        let resultSync = await sqlQuery(sqlStr, [lessPriceCount, roomAllType.toString(), houseId]);
        await sqlQuery(sqlStrBackUps, [lessPriceCount, roomAllType.toString(), houseId]);
        res.send({
            code: resultSync.affectedRows == 1 ? 200 : 400,
            message: resultSync.affectedRows == 1 ? "公寓信息同步成功" : "公寓信息同步失败"
        });
    }else{
        res.send({
            code: 400,
            message: "公寓信息没有匹配的房间"
        });
    }
});
module.exports = router;
