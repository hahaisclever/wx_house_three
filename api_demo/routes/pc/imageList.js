var express = require("express");
var router = express.Router();
var fs = require("fs");
// 引入数据库模块
const sqlQuery = require("../../model/mysql");
const multiparty = require("multiparty"); //获取上传的图片功能

// 上传和删除的图片的路径都需要抽离出来，防止上传服务器后不生效问题
// const imagesUrlLocal = "http://127.0.0.1:3000/";
const imagesUrlLocal = "http://110.42.186.39:3030/";

// 1. 获取图片列表
router.post("/getImageList", async (req, res) => {
    let sqlStr = "select * from imglist";
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "图片列表搜索成功" : "图片列表搜索失败",
        data: result
    });
});

// 2. 上传图片 并保存路径等信息到数据库中
router.post("/uploadImageInfo", async (req, res) => {
    var form = new multiparty.Form();
    const imgId = "ID" + Date.now();
    form.uploadDir = "public/upload/images";
    //上传图片保存的地址(目录必须存在)
    form.parse(req, async (err, fields, files) => {
        // 1、fields:获取表单的数据 2、files：图片上传成功返回的信息
        var url = imagesUrlLocal + files.file[0].path;
        let addSql = "INSERT INTO imglist(imgId,imgUrl) VALUES(?,?)";
        let result = await sqlQuery(addSql, [imgId, url]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "图片添加成功" : "图片添加失败"
        });
    });
});

// 3. 删除数据库数据，并删除本地图片
router.post("/deleteImageInfo", async (req, res) => {
    const { imgId, imgUrl } = req.body;
    const delPath = "./public/upload/images/" + imgUrl;
    try {
        /**
         * @des 判断文件或文件夹是否存在
         */
        if (fs.existsSync(delPath)) {
            fs.unlinkSync(delPath);
            let sqlStr = "delete from imglist where imgId=?";
            let result = await sqlQuery(sqlStr, [imgId]);
            res.send({
                code: result.affectedRows >= 1 ? 200 : 400,
                message: result.affectedRows >= 1 ? "图片删除成功" : "图片删除失败"
            });
        } else {
            res.send({
                code: 400,
                message: delPath + "该文件没找到"
            });
        }
    } catch (error) {
        res.send({
            code: 400,
            message: "del error" + error
        });
    }
});

// 4. 修改图片备注等信息
router.post("/editImageInfo", async (req, res) => {
    // imgType: 0指3D图，1指展示图
    const { imgId, imgName, imgRoomName, imgType } = req.body;
    let sqlStr = "UPDATE imglist SET imgName=?, imgRoomName=?, imgType=? WHERE imgId=?";
    let result = await sqlQuery(sqlStr, [imgName, imgRoomName, imgType, imgId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "上传图片信息成功" : "上传图片信息失败"
    });
});

module.exports = router;
