var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");
// 生成token
const jwt = require("jsonwebtoken");
// 加密账号数据模块
const crypto = require("crypto");
// 加密方法
function encrypt(str) {
  let salt = "wwwccczzz";
  let obj = crypto.createHash("md5");
  str = salt + str;
  obj.update(str);
  return obj.digest("hex");
}

/** 该路由文件，额外包含搜索微信用户列表*/
// 1. 后台管理用户 增加注册
router.post("/addManagerUser", async (req, res) => {
  // 获取注册页面的账号密码
  let username = req.body.username;
  let password = req.body.password;
  const uid = "UID" + Date.now();
  // 判断用户名是否已经注册过了
  let sqlStr = "select * from user where username = ?";
  let result = await sqlQuery(sqlStr, [username]);
  if (result.length != 0) {
    // 该用户已存在，请直接登录或找回密码
    res.send({
      code: 401,
      message: "该用户已存在，请直接登录或找回密码!",
    });
  } else {
    let sqlStrReg = "insert into user (username,password,uid) values (?,?,?)";
    let resultReg = await sqlQuery(sqlStrReg, [
      username,
      encrypt(password),
      uid,
    ]);
    // 这里待补充登录成功消息提示
    res.send({
      code: resultReg.affectedRows >= 1 ? 200 : 400,
      message:
        resultReg.affectedRows >= 1 ? "添加新用户成功" : "添加新用户失败",
    });
  }
});

// 2. 后台管理用户 删除（建议不使用）
router.post("/deleteManagerUser", async (req, res) => {
  const { uid } = req.body;
  let sqlStr = "delete from user where uid=?";
  let result = await sqlQuery(sqlStr, [uid]);
  res.send({
    code: result.affectedRows == 1 ? 200 : 400,
    message:
      result.affectedRows == 1
        ? "后台管理用户删除成功"
        : "后台管理用户删除失败",
  });
});

// 3. 后台管理用户 修改密码
router.post("/editManagerUserPassword", async (req, res) => {
  const { username, password } = req.body;
  let sqlStr = "UPDATE user SET password=? WHERE username=?";
  let result = await sqlQuery(sqlStr, [encrypt(password), username]);
  res.send({
    code: result.affectedRows == 1 ? 200 : 400,
    message:
      result.affectedRows == 1
        ? "后台管理用户修改密码成功"
        : "后台管理用户修改密码失败",
  });
});

// 4. 后台管理用户 查看
router.post("/getManagerUser", async (req, res) => {
  const { username } = req.body;
  let sqlStr =
    "SELECT * FROM `user` WHERE 1=1 AND (username LIKE '?%' OR ?='') AND uid!='admin'";
  let result = await sqlQuery(sqlStr, [username, username]);
  res.send({
    code: result.length >= 0 ? 200 : 400,
    message: result.length >= 0 ? "用户列表搜索成功" : "用户列表搜索失败",
    data: result,
  });
});

// 5. 后台管理用户 禁用
router.post("/disableManagerUser", async (req, res) => {
  const { username } = req.body;
  let sqlStr = "UPDATE user SET status=? WHERE username=?";
  let result = await sqlQuery(sqlStr, [1, username]);
  res.send({
    code: result.affectedRows >= 1 ? 200 : 400,
    message: result.affectedRows >= 1 ? "用户成功禁用" : "用户禁用失败",
  });
});

// 6. 后台管理用户 解禁
router.post("/ableManagerUser", async (req, res) => {
  const { username } = req.body;
  let sqlStr = "UPDATE user SET status=? WHERE username=?";
  let result = await sqlQuery(sqlStr, [0, username]);
  res.send({
    code: result.affectedRows >= 1 ? 200 : 400,
    message: result.affectedRows >= 1 ? "用户成功禁用" : "用户禁用失败",
  });
});

// 7. 后台管理用户 登录
router.post("/loginManagerUser", async (req, res) => {
  const { username, password } = req.body;
  let sqlStr = "select * from user where username = ? and password = ? and status = '0'";
  let result = await sqlQuery(sqlStr, [username, encrypt(password)]);
  if (result.length == 0) {
    res.send({
      code: 401,
      message: "该用户被禁用，或者密码错误",
    });
  } else {
    const SECRET_KEY = "wcz222666";
    const token = jwt.sign(
      {
        // Payload 部分，官方提供七个字段这边省略，可以携带一些可以识别用户的信息。例如 userId。
        // 千万不要是用敏感信息，例如密码，Payload 是可以解析出来的。
        userId: result[0].id,
        role: result[0].username,
      },
      SECRET_KEY,
      {
        expiresIn: "3600s", // token有效期 1小时
        algorithm: "HS256", // 默认使用 "HS256" 算法
      }
    );
    if (result[0].status == "1") {
      res.send({
        code: 401,
        message: "用户违规禁用，请联系管理员",
      });
      return;
    }
    res.send({
      code: 200,
      message: "登录成功",
      token, // 这里需要返回token
      data: result[0],
    });
  }
});

// 8. 微信用户 查看 模糊查找微信名 (额外接口,不属于后台管理人员接口部分)
router.post("/getWXUserList", async (req, res) => {
  const { nickName } = req.body;
  let sqlStr = `SELECT * FROM wxuser WHERE nickName LIKE "%${nickName}%"`;
  let result = await sqlQuery(sqlStr);
  res.send({
    code: result.length > 0 ? 200 : 400,
    message: result.length > 0 ? "微信用户列表搜索成功" : "暂无微信用户列表",
    data: result,
  });
});

module.exports = router;
