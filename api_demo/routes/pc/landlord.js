var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");

// 1. 添加新房东
router.post("/addLandLord", async (req, res) => {
    const landlordId = "LD" + Date.now();
    const { landlordName, landlordPhone, landlordDetail } = req.body;
    let sqlStr = "INSERT INTO landlorduser(landlordId, landlordName, landlordPhone, landlordDetail) VALUES(?,?,?,?)";
    let result = await sqlQuery(sqlStr, [landlordId, landlordName, landlordPhone, landlordDetail]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "添加新房东成功" : "添加新房东失败"
    });
});

// 2. 删除房东信息（建议不使用）
router.post("/deleteLandLord", async (req, res) => {
    const { id } = req.body;
    let sqlStr = "delete from landlorduser where id=?";
    let result = await sqlQuery(sqlStr, [id]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "房东删除成功" : "房东删除失败"
    });
});

// 3. 修改房东信息
router.post("/editLandLord", async (req, res) => {
    const { landlordName, landlordPhone, landlordDetail, landlordId } = req.body;
    let sqlStr = "UPDATE landlorduser SET landlordName=?, landlordPhone=?, landlordDetail=? WHERE landlordId=?";
    let result = await sqlQuery(sqlStr, [landlordName, landlordPhone, landlordDetail, landlordId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "修改房东信息成功" : "修改房东信息失败"
    });
});

// 4. 查找房东信息
router.post("/getLandLord", async (req, res) => {
    const { landlordName } = req.body;
    let sqlStr = `SELECT * FROM landlorduser WHERE landlordName LIKE "%${landlordName}%"`;
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "查找房东信息成功" : "查找房东信息失败",
        data: result
    });
});

module.exports = router;
