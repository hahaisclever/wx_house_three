var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");

// 1. 添加新房间信息
router.post("/addRoomInfo", async (req, res) => {
    const roomId = "RD" + Date.now();
    const {
        houseId,
        roomRent,
        roomRentType,
        roomTitle,
        roomType,
        roomMainUrl,
        facilityList
    } = req.body;
    if (houseId && roomRent && roomRentType && roomTitle && roomType && roomMainUrl && facilityList) {
        let sqlStr = "INSERT INTO roomlist(roomId, houseId, roomRent, roomRentType, roomTitle, roomType, roomMainUrl, facilityList) VALUES(?,?,?,?,?,?,?,?)";
        let result = await sqlQuery(sqlStr, [roomId, houseId, roomRent, roomRentType, roomTitle, roomType, roomMainUrl, facilityList]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "添加新房间成功" : "添加新房间失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 2. 修改新房间信息
router.post("/editRoomInfo", async (req, res) => {
    const {
        roomId,
        houseId,
        roomRent,
        roomRentType,
        roomTitle,
        roomType,
        roomMainUrl,
        facilityList
    } = req.body;
    if (houseId && roomRent && roomRentType && roomTitle && roomType && roomMainUrl && roomId && facilityList) {
        let sqlStr = "UPDATE roomlist SET houseId=?, roomRent=?, roomRentType=?, roomTitle=?, roomType=?, roomMainUrl=?, facilityList=? WHERE roomId=?";
        let result = await sqlQuery(sqlStr, [houseId, roomRent, roomRentType, roomTitle, roomType, roomMainUrl, facilityList, roomId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "修改新房间信息成功" : "修改新房间信息失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 3. 删除新房间信息（不推荐使用）
router.post("/deleteRoomInfo", async (req, res) => {
    const {
        roomId
    } = req.body;
    if (roomId) {
        let sqlStr = "DELETE FROM roomlist WHERE roomId=?";
        let result = await sqlQuery(sqlStr, [roomId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "房间删除成功" : "房间删除失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 4. 查找所有房间信息
router.post("/getRoomInfo", async (req, res) => {
    const {
        roomTitle
    } = req.body;
    let sqlStr = `SELECT * FROM roomlist WHERE roomTitle LIKE "%${roomTitle}%"`;
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "查找房间信息成功" : "查找房间信息失败",
        data: result
    });
});

// 5. 给已创建好的新房间添加、修改、删除设施列表
router.post("/addRoomFacilityListInfo", async (req, res) => {
    const {
        roomId,
        facilityList
    } = req.body;
    if (roomId && facilityList) {
        let sqlStr = "UPDATE roomlist SET facilityList=? WHERE roomId=?";
        let result = await sqlQuery(sqlStr, [facilityList, roomId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "添加新房间设施成功" : "添加新房间设施失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 6. 给已创建好的新房间添加新空间3D建模类型
router.post("/addRoomPlaceListInfo", async (req, res) => {
    const roomPlaceId = "RPD" + Date.now();
    const {
        roomId,
        roomPlaceName,
        roomPlaceImg,
        mark
    } = req.body;
    if (roomId && roomPlaceName && roomPlaceImg) {
        let sqlStr = "INSERT INTO roomplacelist(roomId, roomPlaceId, roomPlaceName, roomPlaceImg, mark) VALUES(?,?,?,?,?)";
        let result = await sqlQuery(sqlStr, [roomId, roomPlaceId, roomPlaceName, roomPlaceImg, mark ? mark : ""]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "添加房间详细建模信息成功" : "添加房间详细建模信息失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 7. 修改公寓中空间3D建模类型信息
// mark: 此处修改房间建模信息需要提供公寓id和房间id，
// mark: 便于房间和公寓1对1的关系。
// mark: 后续房间和公寓是1对多关系，需补充仅根据roomPlaceId就能修改信息的接口。

router.post("/editRoomPlaceListInfo", async (req, res) => {
    const {
        roomId,
        roomPlaceName,
        roomPlaceImg,
        mark,
        roomPlaceId
    } = req.body;
    if (roomId && roomPlaceName && roomPlaceImg && roomPlaceId) {
        let sqlStr = "UPDATE roomplacelist SET roomPlaceName=?, roomPlaceImg=?, mark=? WHERE roomId=? AND roomPlaceId=?";
        let result = await sqlQuery(sqlStr, [roomPlaceName, roomPlaceImg, mark ? mark : "", roomId, roomPlaceId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "修改房间详细建模信息成功" : "修改房间详细建模信息失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 8. 删除公寓中空间3D建模类型信息
router.post("/deleteRoomPlaceListInfo", async (req, res) => {
    const {
        roomId,
        roomPlaceId
    } = req.body;
    if (roomId && roomPlaceId) {
        let sqlStr = `DELETE FROM roomplacelist WHERE roomId=? AND roomPlaceId=?`;
        let result = await sqlQuery(sqlStr, [roomId, roomPlaceId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "房间详细建模信息删除成功" : "房间详细建模信息删除失败"
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 9. 查找公寓中空间3D建模类型信息
router.post("/getRoomPlaceListInfo", async (req, res) => {
    const {
        roomId
    } = req.body;
    if (roomId) {
        let sqlStr = `SELECT * FROM roomplacelist WHERE roomId = ?`;
        let result = await sqlQuery(sqlStr, [roomId]);
        res.send({
            code: result.length > 0 ? 200 : 400,
            message: result.length > 0 ? "房间详细建模信息获取成功" : "房间详细建模信息获取为空",
            data: result
        });
    } else {
        res.send({
            code: 400,
            message: "传递参数不完整"
        });
    }
});

// 10. 查找所有的空间3D建模类型信息
router.post("/getAllRoomPlaceListInfo", async (req, res) => {
    const {
        roomPlaceName
    } = req.body;
    let sqlStr = `SELECT * FROM roomplacelist WHERE roomPlaceName LIKE "%${roomPlaceName}%"`;
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "房间详细建模信息获取成功" : "房间详细建模信息获取为空",
        data: result
    });
});

// 11. 添加关联房间和图片
router.post("/relationRoomAndImage", async (req, res) => {
    const {
        roomId,
        houseId,
        imgId
    } = req.body;
    let sqlStr = `SELECT * FROM roomimgslist WHERE roomId=? AND imgId=?`;
    let exitList = await sqlQuery(sqlStr, [roomId, imgId]);
    if (exitList.length == 0) {
        let addStr = "INSERT INTO roomimgslist(roomId, houseId, imgId) VALUES(?,?,?)"
        let result = await sqlQuery(addStr, [roomId, houseId, imgId]);
        res.send({
            code: result.affectedRows == 1 ? 200 : 400,
            message: result.affectedRows == 1 ? "房间与图片关联成功" : "房间与图片关联失败"
        });
    } else {
        res.send({
            code: 400,
            message: "房间与图片已经存在关联",
        });
    }
});

// 12. 查看关联房间和图片
router.post("/getRelationRoomAndImage", async (req, res) => {
    const {
        roomId,
        houseId
    } = req.body;
    let sqlStr = `SELECT roomimgslist.imgId, roomimgslist.id, roomimgslist.roomId, roomimgslist.isExit, imglist.imgUrl, imglist.imgName, imglist.imgRoomName, imglist.imgType FROM roomimgslist INNER JOIN imglist ON roomimgslist.imgId = imglist.imgId WHERE roomimgslist.roomId =? AND roomimgslist.houseId =?`;
    let result = await sqlQuery(sqlStr, [roomId, houseId]);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "查看关联房间和图片成功" : "查看关联房间和图片获取为空",
        data: result
    });
});

// 13. 删除关联房间和图片
router.post("/deleteRelationRoomAndImage", async (req, res) => {
    const {
        roomId,
        imgId
    } = req.body;
    let sqlStr = `DELETE FROM roomimgslist WHERE roomId=? AND imgId=?`;
    let result = await sqlQuery(sqlStr, [roomId, imgId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "删除关联房间和图片成功" : "删除关联房间和图片失败"
    });
})

// 14. 设置房间仅为图片
router.post("/setRoomOnlyPic", async (req, res) => {
    const {
        roomId,
        houseId
    } = req.body;
    let sqlStr = "UPDATE roomlist SET onlyImg=1 WHERE roomId=? AND houseId=?";
    let result = await sqlQuery(sqlStr, [roomId, houseId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "设置房间仅为图片成功" : "设置房间仅为图片失败"
    });
})

// 15. 取消设置房间仅为图片
router.post("/cancelSetRoomOnlyPic", async (req, res) => {
    const {
        roomId,
        houseId
    } = req.body;
    let sqlStr = "UPDATE roomlist SET onlyImg=0 WHERE roomId=? AND houseId=?";
    let result = await sqlQuery(sqlStr, [roomId, houseId]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "设置房间仅为图片成功" : "设置房间仅为图片失败"
    });
})

module.exports = router;