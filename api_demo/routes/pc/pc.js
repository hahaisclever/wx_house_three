var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");

// 1. 获取点标列表
router.post("/getAllPcList", async (req, res) => {
    let sqlStr = "select * from pclist";
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "点标列表搜索成功" : "点标列表搜索失败",
        data: result
    });
});

// 2. 关联点标和房间
router.post("/relationPcTips", async (req, res) => {
    const { pc_id, tips_id } = req.body;
    let sqlStr = "select * from tips_pc_relation where pc_id=? and tips_id=?";
    let result = await sqlQuery(sqlStr, [pc_id, tips_id]);
    if (result.length != 0) {
        res.send({
            code: 400,
            message: "点标关联失败,重复添加"
        });
        return;
    } else {
        let sqlAddStr = "INSERT INTO tips_pc_relation(pc_id, tips_id) VALUES(?,?)";
        let resultAdd = await sqlQuery(sqlAddStr, [pc_id, tips_id]);
        res.send({
            code: resultAdd.affectedRows == 1 ? 200 : 400,
            message: resultAdd.affectedRows == 1 ? "点标关联房间成功" : "点标关联房间失败"
        });
    }
});

// 3. 删除关联点标列表
router.post("/deleteRelationPcTips", async (req, res) => {
    const { pc_id, tips_id } = req.body;
    let sqlStr = "delete from tips_pc_relation where pc_id=? and tips_id=?";
    let result = await sqlQuery(sqlStr, [pc_id, tips_id]);
    res.send({
        code: result.affectedRows >= 1 ? 200 : 400,
        message: result.affectedRows >= 1 ? "关联点标删除成功" : "关联点标删除失败"
    });
});

// 4. 添加新点标信息
router.post("/addNewPcList", async (req, res) => {
    const pc_id = "pc_" + Date.now();
    const { x, y, z, title, text, image, showTip, showTitle } = req.body;
    console.log(x, y, z, title, text, image, showTip, showTitle);
    let sqlStr = "INSERT INTO pclist(pc_id, x, y, z, title, text, image, showTip, showTitle) VALUES(?,?,?,?,?,?,?,?,?)";
    let result = await sqlQuery(sqlStr, [pc_id, x, y, z, title, text, image, showTip, showTitle]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "添加新点标信息成功" : "添加新点标信息失败"
    });
});

// 5. 修改点标信息
router.post("/editPcList", async (req, res) => {
    const { pc_id, x, y, z, title, text, image, showTip, showTitle } = req.body;
    let sqlStr = "UPDATE pclist SET x=?, y=? ,z=? ,title=? ,text=? ,image=? ,showTip=? ,showTitle=? WHERE pc_id=?";
    let result = await sqlQuery(sqlStr, [x, y, z, title, text, image, showTip, showTitle, pc_id]);
    res.send({
        code: result.affectedRows > 0 ? 200 : 400,
        message: result.affectedRows > 0 ? "修改新点标信息成功" : "修改新点标信息失败"
    });
});

// 6. 删除点标信息
router.post("/deletePcList", async (req, res) => {
    const { pc_id } = req.body;
    let sqlStr = "delete from pclist where pc_id=?";
    let sqlTipsStr = "delete from tips_pc_relation where pc_id=?";
    // 删除关联信息
    await sqlQuery(sqlTipsStr, [pc_id]);
    let result = await sqlQuery(sqlStr, [pc_id]);
    res.send({
        code: result.affectedRows >= 1 ? 200 : 400,
        message: result.affectedRows >= 1 ? "该点标删除成功" : "该点标删除失败"
    });
});

module.exports = router;
