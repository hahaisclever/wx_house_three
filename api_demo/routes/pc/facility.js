var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");

// 1. 设施图标 查看
router.post("/getFacilityList", async (req, res) => {
    const { facilityName } = req.body;
    let sqlStr = `SELECT * FROM facilitylist WHERE facilityName LIKE "%${facilityName}%"`;
    let result = await sqlQuery(sqlStr);
    res.send({
        code: result.length > 0 ? 200 : 400,
        message: result.length > 0 ? "设施图标列表搜索成功" : "暂无设施图标列表",
        data: result
    });
});

// 2. 设施图标 删除(不建议使用)
router.post("/deleteFacility", async (req, res) => {
    const { id } = req.body;
    let sqlStr = "delete from facilitylist where id=?";
    let result = await sqlQuery(sqlStr, [id]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "设施图标删除成功" : "设施图标删除失败"
    });
});

// 3. 设施图标 修改
router.post("/editFacility", async (req, res) => {
    const { facilityName, facilityUrl, facilityType, id } = req.body;
    let sqlStr = "UPDATE facilitylist SET facilityName=? , facilityUrl=? ,facilityType=? WHERE id=?";
    let result = await sqlQuery(sqlStr, [facilityName, facilityUrl, facilityType, id]);
    res.send({
        code: result.affectedRows == 1 ? 200 : 400,
        message: result.affectedRows == 1 ? "设施图标修改成功" : "设施图标修改失败"
    });
});

// 4. 设施图标 增加
router.post("/addFacility", async (req, res) => {
    const { facilityName, facilityType, facilityUrl } = req.body;
    let sqlStr = "select * from facilitylist where facilityName = ?";
    let result = await sqlQuery(sqlStr, [facilityName]);
    if (result.length != 0) {
        // 该用户已存在，请直接登录或找回密码
        res.send({
            code: 401,
            message: "该设施图标已存在"
        });
    } else {
        const facilityId = "FD" + Date.now();
        let sqlStr = "insert into facilitylist (facilityName,facilityType,facilityUrl,facilityId) values (?,?,?,?)";
        let resultReg = await sqlQuery(sqlStr, [facilityName, facilityType, facilityUrl, facilityId]);
        res.send({
            code: resultReg.affectedRows >= 1 ? 200 : 400,
            message: resultReg.affectedRows >= 1 ? "添加新设施图标成功" : "添加新设施图标失败"
        });
    }
});

module.exports = router;
