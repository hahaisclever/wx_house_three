var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");
const JwtUtil = require("../../model/jwt");
const jwt = require("jsonwebtoken");
// 加密账号数据模块
const crypto = require("crypto");
const axios = require("axios");
// 加密方法
function encrypt(str) {
    let salt = "wwwccczzz";
    let obj = crypto.createHash("md5");
    str = salt + str;
    obj.update(str);
    return obj.digest("hex");
}

/* GET home page. */
router.get("/register", async function (req, res, next) {
    res.send("注册模块");
});

router.get("/login", async function (req, res, next) {
    res.send("登录模块");
});

router.get("/search", async function (req, res, next) {
    let sqlStr = "select * from user";
    let result = await sqlQuery(sqlStr);
    if (result.length != 0) {
        res.send(result);
    }
});

router.post("/register", async (req, res) => {
    // 获取注册页面的账号密码
    let username = req.body.username;
    let password = req.body.password;
    const uid = "UID" + Date.now();
    // 判断用户名是否已经注册过了
    let sqlStr = "select * from user where username = ?";
    let result = await sqlQuery(sqlStr, [username]);
    if (result.length != 0) {
        // 该用户已存在，请直接登录或找回密码
        res.send({
            code: 401,
            message: "该用户已存在，请直接登录或找回密码!"
        });
    } else {
        let sqlStrReg = "insert into user (username,password,uid) values (?,?,?)";
        let resultReg = await sqlQuery(sqlStrReg, [username, encrypt(password), uid]);
        // 这里待补充登录成功消息提示
        res.send({
            code: resultReg.affectedRows >= 1 ? 200 : 400,
            message: resultReg.affectedRows >= 1 ? "添加新用户成功" : "添加新用户失败"
        });
    }
});

// 登陆成功获得token
router.post("/login", async (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let sqlStr = "select * from user where username = ? and password = ?";
    let result = await sqlQuery(sqlStr, [username, encrypt(password)]);
    if (result.length == 0) {
        res.send({
            code: 401,
            message: "该用户不存在，或者密码错误"
        });
    } else {
        const SECRET_KEY = "wcz222666";
        const token = jwt.sign(
            {
                // Payload 部分，官方提供七个字段这边省略，可以携带一些可以识别用户的信息。例如 userId。
                // 千万不要是用敏感信息，例如密码，Payload 是可以解析出来的。
                userId: result[0].id,
                role: result[0].username
            },
            SECRET_KEY,
            {
                expiresIn: "3600s", // token有效期 1小时
                algorithm: "HS256" // 默认使用 "HS256" 算法
            }
        );
        if (result[0].status == "1") {
            res.send({
                code: 401,
                message: "用户违规禁用，请联系管理员"
            });
            return;
        }
        res.send({
            code: 200,
            message: "登录成功",
            token, // 这里需要返回token
            data: result[0]
        });
    }
});

// 2022-05-24补充到后台服务器  ！！！1. 该接口也需要免token校验 2. 需要下载和引入axios！！！
// 小程序登录登陆成功获得token 升级版，把获取openId和用户信息定义在后台

// 2022年，该小程序接口不再返回 avatarUrl, nickName, gender 需要再做修改
router.post("/wx_login_v1", async (req, res) => {
    let { code, avatarUrl, nickName, gender } = req.body;
    const params = {
        //填上自己的小程序唯一标识
        appid: "wx31544c6f726f94fa",
        //填上自己的小程序的 app secret
        secret: "1f652e37a04d201b69c1a496b09fc165",
        grant_type: "authorization_code",
        js_code: code
    };
    axios.get("https://api.weixin.qq.com/sns/jscode2session", { params }).then(async (response) => {
        // res.data 是服务器返回的数据
        if (response.data) {
            const { session_key, openid } = response.data;
            // console.log(response.data, session_key, openid, avatarUrl, nickName, gender);
            if(!openid) return
            let sqlStr = "select * from wxuser where openid=?";
            let result = await sqlQuery(sqlStr, [openid]);
            // 该openid没注册的话就自动注册
            const uid = "UID" + Date.now();
            if (result.length == 0) {
                let addSqlStr = "INSERT INTO wxuser(uid, openid, avatarUrl, nickName, gender) VALUES(?,?,?,?,?)";
                await sqlQuery(addSqlStr, [uid, openid, avatarUrl, nickName, gender]);
            } else {
                // 该openid有注册的话就更新信息
                let editSqlStr = "UPDATE wxuser SET avatarUrl=?, nickName=?, gender=? WHERE openid=?";
                await sqlQuery(editSqlStr, [avatarUrl, nickName, gender, openid]);
            }
            // 看是否需要返回token
            const SECRET_KEY = "wcz222666";
            const token = jwt.sign(
                {
                    // Payload 部分，官方提供七个字段这边省略，可以携带一些可以识别用户的信息。例如 userId。
                    // 千万不要是用敏感信息，例如密码，Payload 是可以解析出来的。
                    userId: openid,
                    role: nickName
                },
                SECRET_KEY,
                {
                    expiresIn: "3600s", // token有效期 1小时
                    algorithm: "HS256" // 默认使用 "HS256" 算法
                }
            );
            res.send({
                code: 206,
                message: "登录成功",
                token, // 这里需要返回token 和 用户信息(不返回用户信息也可以，因小程序用户信息保存使用 getUserProfile 存在本地的)
                userInfo: {
                    avatarUrl,
                    nickName,
                    gender,
                    uid: result.length == 0 ? uid : result[0].uid,
                }
            });
        }
    });
});

// 小程序登录登陆成功获得token
router.post("/wx_login", async (req, res) => {
    let { openid, avatarUrl, nickName, gender } = req.body;
    console.log(openid, avatarUrl, nickName, gender);
    let sqlStr = "select * from wxuser where openid=?";
    let result = await sqlQuery(sqlStr, [openid]);
    if (result.length == 0) {
        const uid = "UID" + Date.now();
        let addSqlStr = "INSERT INTO wxuser(uid, openid, avatarUrl, nickName, gender) VALUES(?,?,?,?,?)";
        await sqlQuery(addSqlStr, [uid, openid, avatarUrl, nickName, gender]);
    } else {
        let editSqlStr = "UPDATE wxuser SET avatarUrl=?, nickName=?, gender=? WHERE openid=?";
        await sqlQuery(editSqlStr, [avatarUrl, nickName, gender, openid]);
    }
    const SECRET_KEY = "wcz222666";
    const token = jwt.sign(
        {
            // Payload 部分，官方提供七个字段这边省略，可以携带一些可以识别用户的信息。例如 userId。
            // 千万不要是用敏感信息，例如密码，Payload 是可以解析出来的。
            userId: openid,
            role: nickName
        },
        SECRET_KEY,
        {
            expiresIn: "3600s", // token有效期 10小时
            algorithm: "HS256" // 默认使用 "HS256" 算法
        }
    );
    res.send({
        code: 206,
        message: "登录成功",
        token, // 这里需要返回token
        userInfo: {
            avatarUrl,
            nickName,
            gender
        }
    });
});

//自动登录功能
router.post("/user", (req, res) => {
    res.send({
        code: 200,
        message: "token未过期，仍可使用"
    });
});

// 自动登录功能
router.post("/wx_user", (req, res) => {
    res.send({
        code: 200,
        message: "token未过期，仍可使用"
    });
});

// 返回非明文的AppSecret
router.post("/getAppSecret", (req, res) => {
    res.send({
        code: 200,
        message: "返回AppSecret",
        appid: "wx31544c6f726f94fa",
        secret: "1f652e37a04d201b69c1a496b09fc165"
    });
});

module.exports = router;
