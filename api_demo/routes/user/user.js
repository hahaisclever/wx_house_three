var express = require("express");
var router = express.Router();
// 引入数据库模块
const sqlQuery = require("../../model/mysql");

// 1. 模糊查找 获取用户列表 不返回管理员账号
router.post("/getUserList", async (req, res) => {
    const { username } = req.body;
    let sqlStr = "SELECT * FROM `user` WHERE 1=1 AND (username LIKE '?%' OR ?='') AND uid!='admin'";
    let result = await sqlQuery(sqlStr, [username, username]);
    res.send({
        code: result.length >= 0 ? 200 : 400,
        message: result.length >= 0 ? "用户列表搜索成功" : "用户列表搜索失败",
        data: result
    });
});

// // 2. 添加新的用户，用户名 / 账号 不能重复
// router.post("/addNewUser", async (req, res) => {
//     const { username, password } = req.body;
//     const sqlOnly = "select * from user where username=?";
//     const resultOnly = await sqlQuery(sqlOnly, [username]);
//     if (resultOnly.length != 0) {
//         res.send({
//             code: 400,
//             message: "该用户已存在"
//         });
//     }
//     const uid = "UID" + Date.now();
//     let sqlStr = "INSERT INTO user(username, password, uid) VALUES(?,?,?)";
//     let result = await sqlQuery(sqlStr, [username, password, uid]);
//     res.send({
//         code: result.affectedRows >= 1 ? 200 : 400,
//         message: result.affectedRows >= 1 ? "添加新用户成功" : "添加新用户失败"
//     });
// });

// 3. 修改用户信息
router.post("/editUserInfo", async (req, res) => {
    const { username, password, uid } = req.body;
    console.log(username, password, uid);
    let sqlStr = "UPDATE user SET username=?, password=? WHERE uid=?";
    let result = await sqlQuery(sqlStr, [username, password, uid]);
    res.send({
        code: result.affectedRows > 0 ? 200 : 400,
        message: result.affectedRows > 0 ? "用户信息修改成功" : "用户信息修改失败"
    });
});

// 4. 删除用户
router.post("/deleteUserInfo", async (req, res) => {
    const { username, uid } = req.body;
    let sqlStr = "delete from user where username=? and uid=?";
    let result = await sqlQuery(sqlStr, [username, uid]);
    res.send({
        code: result.affectedRows >= 1 ? 200 : 400,
        message: result.affectedRows >= 1 ? "用户信息删除成功" : "用户信息删除失败"
    });
});

// 5. 禁用用户
router.post("/disableUserInfo", async (req, res) => {
    const { username, uid, status } = req.body;
    let sqlStr = "UPDATE user SET status=? where username=? and uid=?";
    let result = await sqlQuery(sqlStr, [status, username, uid]);
    res.send({
        code: result.affectedRows >= 1 ? 200 : 400,
        message: result.affectedRows >= 1 ? "用户成功解禁或禁用" : "用户解禁或禁用删除"
    });
});

module.exports = router;
