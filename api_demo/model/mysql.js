const mysql = require('mysql');
// 创建数据库连接
const options = {
    host: "localhost",
    port: "3306",
    user: "root",
    password: "222666",
    // database: "wx_house"
    database: "wx_house_production"
}
const con = mysql.createConnection(options); //创建连接对象

//建立连接
con.connect((err)=>{
    //如果建立连接失败
    if(err){
        console.log(err)
    }else{
        console.log('数据库连接成功')
    }
})

function sqlQuery(strSql,arr){
    return new Promise(function(resolve,reject){
        con.query(strSql,arr,(err,results)=>{
            if(err){
                reject(err)
            }else{
                resolve(results)
            }
        })
    })
}

module.exports = sqlQuery;
