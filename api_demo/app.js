var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
// // 引入session模块
// const session = require('express-session')
// 配置上传对象  这里打开会有报错
// const upload = require({dest:"./public/upload"})
// const JwtUtil = require('./model/jwt')

var jwt = require("express-jwt");
var indexRouter = require("./routes/index");
var loginRouter = require("./routes/login/login");
var houseRouter = require("./routes/house/house");
var pcRouter = require("./routes/pc/pc");
var userRouter = require("./routes/user/user");
var landlordRouter = require("./routes/pc/landlord");
var managerRouter = require("./routes/pc/managerUser");
var facilityRouter = require("./routes/pc/facility");
var imageListRouter = require("./routes/pc/imageList");
var houseInfoRouter = require("./routes/pc/houseInfo");
var roomInfoRouter = require("./routes/pc/roomInfo");
var interfaceRouter = require("./routes/wx/interface");

var app = express();
// 解决跨域问题
var cors = require("cors");
app.use(cors());
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// 配置session
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

const SECRET_KEY = "wcz222666";
app.use(
    jwt({
        secret: SECRET_KEY,
        algorithms: ["HS256"]
    }).unless({
        path: ["/", "/rl/login",, { url: /^\/wxInterface\/*/, methods: ["POST"] }, "/rl/wx_login_v1", "/rl/wx_login", "/rl/getAppSecret", "/house/getTipsDetailList", "/rl/register", "/upload", "/uploadimg", { url: /^\/public\/upload\/images\/.*/, methods: ["GET"] }, "/"]
    })
);

// 前台路由
app.use("/", indexRouter);
// 登录注册路由
app.use("/rl", loginRouter);
// 图片等路由注册
app.use("/house", houseRouter);
// 点位列表
app.use("/pc", pcRouter);

// 上面是旧接口，下面是新接口

// 获取用户列表接口
app.use("/user", userRouter);
// 房东信息接口
app.use("/pcLandlord", landlordRouter);
// 后台人员信息接口
app.use("/pcManager", managerRouter);
// 设备图标信息接口
app.use("/pcFacility", facilityRouter);
// 上传图片列表信息接口
app.use("/pcImageList", imageListRouter);
// 公寓信息接口 ;
app.use("/pcHouseInfo", houseInfoRouter);
// 房间信息接口;
app.use("/pcRoomInfo", roomInfoRouter);
// 小程序所有接口
app.use("/wxInterface", interfaceRouter);

// 设置静态文件目录
app.use("/public/upload/images", express.static("public/upload/images"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

module.exports = app;
